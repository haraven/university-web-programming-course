<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Imgur, the simple image sharing website</title>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/themes/css/ui.css" />
    <script src="${pageContext.request.contextPath}/themes/script/js/jquery-2.2.3.min.js"></script>
</head>
<body>
    <script>
        $(document).ready(function () {
           $("#error").fadeOut(5000);
        });
    </script>
    <div id="top-bar">
        <div id="result-filter-wrapper">
            <form method="post" action="${pageContext.request.contextPath}/home">
                <div id="resultcount-group">
                    <span>Display </span>
                    <input name="resultcount" type="number" value="${resultcount}">
                    <span>
                         result(s).
                    </span>
                </div>
                <button type="submit" id="btn-submit">GO</button>
            </form>
        </div>
        <div id="logout-wrapper">
            <form method="post" action="${pageContext.request.contextPath}/logout">
                <button type="submit">LOG OUT</button>
            </form>
        </div>
        <div id="file-upload-wrapper">
            <form action="${pageContext.request.contextPath}/upload" method="post" enctype="multipart/form-data">
                <div id="file-wrapper">
                    <label for="file-input">
                        <span id="btn-upload-span">
                            UPLOAD AN IMAGE
                        </span>
                    </label>
                    <input id="file-input" type="file" name="file-input" multiple="true" onclick="document.getElementById('btn-upload').style.display = 'block'; document.getElementById('btn-upload-span').style.display = 'none';">
                    <button type="submit" id="btn-upload" onclick="document.getElementById('btn-upload').style.display = 'none'; document.getElementById('btn-upload-span').style.display = 'block';">
                        UPLOAD OR CANCEL
                    </button>
                </div>
            </form>
        </div>
    </div>
    <div id="error" style="position: fixed; z-index: 2; top: 2.5em; left: 43%;">
            ${message}
    </div>
    <div id="page-content" style=" position: absolute;
            background-color: #090909;
            width: 1013px;
            top: 5%;
            left: 22%;
            max-height: 2000px;
            align-content: center;
            align-items: center;
            border: 1px solid #1e1e1e">
        <c:forEach items="${pics}" var="item">
            <a title="${item.substring(0, item.lastIndexOf('.') - 1)}" href="${pageContext.request.contextPath}/gallery/${item}">
                <div id="post" style="background: #0e0e0e url(${pageContext.request.contextPath}/pictures/${item}) center center;
                    width: 180px;
                    height: 180px;
                    float: left;
                    overflow: hidden;
                    border: 2px solid #1e1e1e;
                    margin: 1em 0.5em 1em 1em;">
                </div>
            </a>
        </c:forEach>
    </div>
</body>
</html>