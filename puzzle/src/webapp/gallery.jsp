<%@ page import="model.UserVote" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Optional" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>${picture.getTitle().substring(0, picture.getTitle().indexOf('.') - 1)}</title>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/themes/css/ui.css" />
    <script src="${pageContext.request.contextPath}/themes/script/js/jquery-2.2.3.min.js"></script>
</head>
<body>
    <script>
        $(document).ready(function () {
            $("#thumbnail").on('click', function() {
              var img = $('<img />', {
                  src: this.src,
                  'class': 'fullsize-image'
              });
              $('#fullsize-image-container').html(img).show();
           });
            $("#fullsize-image-container").on('click', function() {
                $(this).hide();
            });
        });
    </script>
    <div id="top-bar">
        <div id="back-wrapper">
            <a href="${pageContext.request.contextPath}/home">
                    BACK
            </a>
        </div>
        <div id="logout-wrapper">
            <form method="post" action="${pageContext.request.contextPath}/logout">
                <button type="submit">LOG OUT</button>
            </form>
        </div>
        <div id="file-upload-wrapper">
            <form action="${pageContext.request.contextPath}/upload" method="post" enctype="multipart/form-data">
                <div id="file-wrapper">
                    <label for="file-input">
                            <span id="btn-upload-span">
                                UPLOAD AN IMAGE
                            </span>
                    </label>
                    <input id="file-input" type="file" name="file-input" multiple="true" onclick="document.getElementById('btn-upload').style.display = 'block'; document.getElementById('btn-upload-span').style.display = 'none';">
                    <button type="submit" id="btn-upload" onclick="document.getElementById('btn-upload').style.display = 'none'; document.getElementById('btn-upload-span').style.display = 'block';">
                        UPLOAD OR CANCEL
                    </button>
                </div>
            </form>
        </div>
    </div>
    <div id="image-post">
        <div id="image-info">
            <p id="image-title">
                ${picture.getTitle().substring(0, picture.getTitle().lastIndexOf('.') - 1)}
            </p>
            <div id="image-poster">
                <span>
                    by
                </span>
                <span id="image-username">
                    ${picture.getOP().getUsername()}
                </span>
            </div>
        </div>
        <div id="image-container">
            <img id="thumbnail" src="${pageContext.request.contextPath}/pictures/${picture.getSource().toString().substring(picture.getSource().toString().lastIndexOf('\\') + 1)}" alt="${picture.getTitle()} could not be retrieved." style="width: 100%; height:100%"/>
        </div>
        <div id="bottom-container">
            <form method="post" action="${pageContext.request.contextPath}/pictures/${picture.getSource().toString().substring(picture.getSource().toString().lastIndexOf('\\') + 1)}">
                <c:choose>
                    <c:when test="${!picture.getOP().equals(user)}">
                        <%
                            Boolean is_downvote;
                            @SuppressWarnings("unchecked")
                            List<UserVote> user_votes = (List<UserVote>) request.getAttribute("user_votes");
                            if (user_votes != null)
                            {
                                Optional<UserVote> user_vote = user_votes.stream()
                                    .filter(userVote -> userVote.getUser().equals(request.getSession().getAttribute("user")) &&
                                            userVote.getPicture().equals(request.getAttribute("picture")))
                                    .findFirst();
                                if (user_vote.isPresent())
                                {
                                    UserVote tmp = user_vote.get();
                                    is_downvote = tmp.isDownvote();
                                }
                                else is_downvote = null;
                            }
                            else is_downvote = null;
                            request.setAttribute("is_downvote", is_downvote);
                        %>
                        <c:choose>
                            <c:when test="${is_downvote != null && is_downvote == true}">
                                <input type="submit" id="upvote" name="btn" value="UPVOTE"/>
                                <input type="submit" id="downvote" name="btn" value="DOWNVOTE" disabled style="color: #b36f1d;"/>
                            </c:when>
                            <c:when test="${is_downvote != null && is_downvote == false}">
                                <input type="submit" id="upvote" name="btn" value="UPVOTE" disabled style="color: #b36f1d;"/>
                                <input type="submit" id="downvote" name="btn" value="DOWNVOTE"/>
                            </c:when>
                            <c:otherwise>
                                <input type="submit" id="upvote" name="btn" value="UPVOTE"/>
                                <input type="submit" id="downvote" name="btn" value="DOWNVOTE"/>
                            </c:otherwise>
                        </c:choose>
                        <%
                            request.removeAttribute("is_downvote");
                        %>
                    </c:when>
                    <c:otherwise>
                        <input type="submit" id="delete" name="btn" value="DELETE">
                    </c:otherwise>
                </c:choose>
            </form>
            <div id="stats">
                <p id="pts">
                    ${picture.getUpvotes()} points
                </p>
                <p>
                    ${picture.getViews()} views
                </p>
            </div>
        </div>
    </div>
    <div id="fullsize-image-container">
    </div>
</body>
</html>
