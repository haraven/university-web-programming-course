package utils.factories;

import model.User;

public class UserFactory {
    public static User makeUser() {
        return new User();
    }

    public static User makeUser(String username, String password) {
        return new User(username, password);
    }
}
