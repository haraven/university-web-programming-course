package utils.factories;

import model.Picture;
import model.User;
import model.UserVote;

public class UserVoteFactory {
    public static UserVote makeUserVote() {
        return new UserVote();
    }

    public static UserVote makeUserVote(User user, Picture picture, Boolean vote_type) {
        return new UserVote(user, picture, vote_type);
    }

    public static UserVote makeUserVote(Integer id, User user, Picture picture, Boolean vote_type) {
        return new UserVote(id, user, picture, vote_type);
    }
}
