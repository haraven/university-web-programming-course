package utils.factories;

import model.Picture;
import model.User;

import java.nio.file.Path;

public class PictureFactory {
    public static Picture makePicture() {
        return new Picture();
    }

    public static Picture makePicture(String title, String source_string, User OP) {
        return new Picture(title, source_string, OP);
    }

    public static Picture makePicture(Integer id, String title, String source_string, User OP) {
        return new Picture(id, title, source_string, OP);
    }

    public static Picture makePicture(String title, String source_string, Integer upvotes, Integer views, User OP) {
        return new Picture(title, source_string, upvotes, views, OP);
    }

    public static Picture makePicture(Integer id, String title, String source_string, Integer upvotes, Integer views, User OP) {
        return new Picture(id, title, source_string, upvotes, views, OP);
    }

    public static Picture makePicture(String title, Path source, User OP) {
        return new Picture(title, source, OP);
    }

    public static Picture makePicture(Integer id, String title, Path source, User OP) {
        return new Picture(id, title, source, OP);

    }

    public static Picture makePicture(Integer id, String title, Path source, Integer upvotes, Integer views, User OP) {
        return new Picture(id, title, source, upvotes, views, OP);
    }

    public static Picture makePicture(String title, Path source, Integer upvotes, Integer views, User OP) {
        return new Picture(title, source, upvotes, views, OP);
    }
}
