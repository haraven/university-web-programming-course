package utils.adapters;

import model.User;
import utils.exceptions.AdapterException;
import utils.factories.UserFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserAdapter extends DataAdapter<User> {
    public class UserRowMapper implements DataAdapter.RowMapper<User> {
        public List<User> mapEntities(ResultSet rs) throws SQLException {
            List<User> users = new ArrayList<>();
            while (rs.next())
                users.add(mapRow(rs));

            return users;
        }

        public User mapRow(ResultSet rs) throws SQLException {
            User user = UserFactory.makeUser();
            user.setEntryID(rs.getInt("EntryID"));
            user.setUsername(rs.getString("Username"))
                    .setPassword(rs.getString("Password"));

            return user;
        }
    }

    public UserAdapter() {
        super();
    }

    @Override
    public RowMapper<User> getRowMapper() {
        return new UserRowMapper();
    }

    public DataAdapter<User> insertOrUpdate(User entity) {
        if (selectWhere(entity.getUsername()) != null)
            update(entity);
        else {
            String sql = "INSERT INTO imgur.user (Username, Password) VALUES(?, ?)";
            Connection connection = openConnection();
            try {
                PreparedStatement statement = connection.prepareStatement(sql);
                statement.setString(1, entity.getUsername());
                statement.setString(2, entity.getPassword());
                statement.executeUpdate();
                connection.close();
            } catch (SQLException e) {
                throw new AdapterException("Error preparing insert statement", e);
            }
        }

        return this;
    }

    @Override
    public List<User> select() {
        String sql = "SELECT * FROM imgur.user";
        Connection connection = openConnection();
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            UserRowMapper user_row_mapper = new UserRowMapper();
            List<User> users = user_row_mapper.mapEntities(statement.executeQuery());
            connection.close();
            return users;
        } catch (SQLException e) {
            throw new AdapterException("Error preparing select statement", e);
        }
    }

    @Override
    public User selectWhere(Object... args) {
        final Integer USERNAME = 0;
        final Integer PASSWORD = 1;

        String username = (String)args[USERNAME];
        String password = args.length > 1 ? (String)args[PASSWORD] : "";
        String sql = "SELECT * FROM imgur.user WHERE Username = ?";
        if (!password.equals(""))
            sql += "AND Password = ?";
        Connection connection = openConnection();
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, username);
            if (!password.equals(""))
                statement.setString(2, password);
            UserRowMapper user_row_mapper = new UserRowMapper();
            List<User> user = user_row_mapper.mapEntities(statement.executeQuery());
            connection.close();
            if (user.isEmpty())
                return null;
            else
                return user.get(0);
        } catch (SQLException e) {
            throw new AdapterException("Error preparing select statement", e);
        }
    }

    @Override
    public User update(User entity) {
        String sql = "UPDATE imgur.user U SET U.Password = ? WHERE U.Username = ?";
        Connection connection = openConnection();
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, entity.getPassword());
            statement.setString(2, entity.getUsername());
            int res = statement.executeUpdate();
            connection.close();
            return res > 0 ? entity : null;
        } catch (SQLException e) {
            throw new AdapterException("Error preparing select statement", e);
        }
    }

    @Override
    public Boolean delete(User entity) {
        String sql = "DELETE U FROM imgur.user U WHERE U.EntryID = ? OR U.Username = ?";
        Connection connection = openConnection();
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, entity.getEntryID());
            statement.setString(2, entity.getUsername());
            int res = statement.executeUpdate();
            connection.close();
            return res > 0;
        } catch (SQLException e) {
            throw new AdapterException("Error preparing delete statement", e);
        }
    }

    @Override
    public Boolean delete(Integer id) {
        String sql = "DELETE U FROM imgur.user U WHERE U.EntryID = ?";
        return genericDelete(sql, id);
    }
}
