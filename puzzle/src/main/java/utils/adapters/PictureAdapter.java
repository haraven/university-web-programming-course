package utils.adapters;

import model.Picture;
import model.User;
import utils.exceptions.AdapterException;
import utils.factories.PictureFactory;

import java.nio.file.Path;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PictureAdapter extends DataAdapter<Picture> {
    public PictureAdapter() { super(); }

    @Override
    public RowMapper<Picture> getRowMapper() {
        return new PictureRowMapper();
    }

    private class PictureRowMapper implements RowMapper<Picture> {
        public Picture mapRow(ResultSet rs) throws SQLException {
            Picture picture = PictureFactory.makePicture();
            picture.setEntryID(rs.getInt("EntryID"));
            picture.setTitle(rs.getString("Title"))
                .setSource(rs.getString("Source"))
                .setUpvotes(rs.getInt("Upvotes"))
                .setViews(rs.getInt("Views"));

            String sql = "SELECT * FROM imgur.user WHERE EntryID = ?";
            Connection connection = openConnection();
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, rs.getInt("UserEntryID"));
            UserAdapter user_adapter = new UserAdapter();
            RowMapper<User> user_row_mapper = user_adapter.getRowMapper();
            List<User> user = user_row_mapper.mapEntities(statement.executeQuery());
            connection.close();
            if (user.isEmpty())
                picture.setOP(null);
            else
                picture.setOP(user.get(0));

            return picture;
        }

        @Override
        public List<Picture> mapEntities(ResultSet rs) throws SQLException {
            List<Picture> pictures = new ArrayList<>();

            while (rs.next())
                pictures.add(mapRow(rs));

            return pictures;
        }
    }

    @Override
    public DataAdapter<Picture> insertOrUpdate(Picture entity) {
        if (selectWhere(entity.getSource()) != null)
            update(entity);
        else {
            String sql = "INSERT INTO imgur.picture (Title, Source, Upvotes, Views, UserEntryID) VALUES(?, ?, ?, ?, ?)";
            Connection connection = openConnection();
            try {
                PreparedStatement statement = connection.prepareStatement(sql);
                statement.setString(1, entity.getTitle());
                statement.setString(2, entity.getSource().toString());
                statement.setInt(3, entity.getUpvotes());
                statement.setInt(4, entity.getViews());
                statement.setInt(5, entity.getOP().getEntryID());
                statement.executeUpdate();
                connection.close();
            } catch (SQLException e) {
                throw new AdapterException("Error preparing insert statement", e);
            }
        }

        return this;
    }

    @Override
    public List<Picture> select() {
        String sql = "SELECT * FROM imgur.picture";
        Connection connection = openConnection();
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            PictureRowMapper picture_row_mapper = new PictureRowMapper();
            List<Picture> pictures = picture_row_mapper.mapEntities(statement.executeQuery());
            connection.close();
            return pictures;
        } catch (SQLException e) {
            throw new AdapterException("Error preparing select statement", e);
        }
    }

    @Override
    public Picture selectWhere(Object... args) {
        final Integer PATH = 0;
        Path path = (Path) args[PATH];
        String sql = "SELECT * FROM imgur.picture WHERE Source = ?";
        Connection connection = openConnection();
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, path.toString());
            PictureRowMapper picture_row_mapper = new PictureRowMapper();
            List<Picture> picture = picture_row_mapper.mapEntities(statement.executeQuery());
            connection.close();
            if (picture.isEmpty())
                return null;
            else
                return picture.get(0);
        } catch (SQLException e) {
            throw new AdapterException("Error preparing select statement", e);
        }
    }

    @Override
    public Picture update(Picture entity) {
        String sql = "UPDATE imgur.picture U SET U.Title = ?, U.Upvotes = ?, U.Views = ?, U.UserEntryID = ? WHERE U.Source = ?";
        Connection connection = openConnection();
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, entity.getTitle());
            statement.setInt(2, entity.getUpvotes());
            statement.setInt(3, entity.getViews());
            statement.setInt(4, entity.getOP().getEntryID());
            statement.setString(5, entity.getSource().toString());
            int res = statement.executeUpdate();
            connection.close();
            return res > 0 ? entity : null;
        } catch (SQLException e) {
            throw new AdapterException("Error preparing select statement", e);
        }
    }

    @Override
    public Boolean delete(Picture entity) {
        String sql = "DELETE U FROM imgur.picture U WHERE U.EntryID = ? OR U.Source = ?";
        Connection connection = openConnection();
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, entity.getEntryID());
            statement.setString(2, entity.getSource().toString());
            int res = statement.executeUpdate();
            connection.close();
            return res > 0;
        } catch (SQLException e) {
            throw new AdapterException("Error preparing delete statement", e);
        }
    }

    @Override
    public Boolean delete(Integer id) {
        String sql = "DELETE U FROM imgur.picture U WHERE U.EntryID = ?";
        return genericDelete(sql, id);
    }
}
