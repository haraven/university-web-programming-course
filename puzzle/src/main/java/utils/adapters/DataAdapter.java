package utils.adapters;
import utils.exceptions.AdapterException;

import java.sql.*;
import java.util.List;

public abstract class DataAdapter<T> {
    private String connection_string;
    private String username;
    private String password;

    private DataAdapter(String connection_string, String username, String password) {
        try {
            Class.forName(com.mysql.cj.jdbc.Driver.class.getName()).newInstance();
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        this.connection_string = connection_string;
        this.username          = username;
        this.password          = password;
    }

    DataAdapter() {
        this("jdbc:mysql://localhost:3306/imgur?autoReconnect=true&useSSL=false&serverTimezone=UTC", "root", "RESILIENCEWILLFIXIT");
    }

    interface RowMapper<T> {
        T mapRow(ResultSet rs) throws SQLException;
        List<T> mapEntities(ResultSet rs) throws SQLException;
    }

    public abstract RowMapper<T> getRowMapper();

    public abstract DataAdapter<T> insertOrUpdate(T entity);
    public abstract List<T> select();
    public abstract T selectWhere(Object... args);
    public abstract T update(T entity);
    public abstract Boolean delete(T entity);
    public abstract Boolean delete(Integer id);

    Boolean genericDelete(String sql, Integer id) {
        Connection connection = openConnection();
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, id);
            int res = statement.executeUpdate();
            connection.close();
            return res > 0;
        } catch (SQLException e) {
            throw new AdapterException("Error preparing delete statement", e);
        }
    }

    Connection openConnection() throws AdapterException {
        try {
            return DriverManager.getConnection(connection_string, username, password);
        } catch (SQLException e) {
            throw new AdapterException("Error opening connection", e);
        }
    }

    public String getConnectionString() {
        return connection_string;
    }

    public void setConnectionString(String connection_string) {
        this.connection_string = connection_string;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
