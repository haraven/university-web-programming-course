package utils.adapters;

import model.Picture;
import model.User;
import model.UserVote;
import utils.exceptions.AdapterException;
import utils.factories.UserVoteFactory;

import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserVoteAdapter extends DataAdapter<UserVote> {
    public UserVoteAdapter() { super(); }

    private class UserVoteRowMapper implements RowMapper<UserVote> {
        @Override
        public UserVote mapRow(ResultSet rs) throws SQLException {
            DataAdapter<User> user_data_adapter = new UserAdapter();
            DataAdapter<Picture> picture_data_adapter = new PictureAdapter();

            UserVote user_vote = UserVoteFactory.makeUserVote();
            user_vote.setEntryID(rs.getInt("EntryID"));
            user_vote.setVoteType(rs.getInt("VoteType") == 1);
            User user = user_data_adapter.selectWhere(rs.getString("Username"));
            Picture picture = picture_data_adapter.selectWhere(Paths.get(rs.getString("PictureSource")));
            user_vote.setPicture(picture)
                    .setUser(user);

            return user_vote;
        }

        @Override
        public List<UserVote> mapEntities(ResultSet rs) throws SQLException {
            List<UserVote> user_votes = new ArrayList<>();
            while(rs.next())
                user_votes.add(mapRow(rs));

            return user_votes;
        }
    }

    @Override
    public RowMapper<UserVote> getRowMapper() {
        return new UserVoteRowMapper();
    }

    @Override
    public DataAdapter<UserVote> insertOrUpdate(UserVote entity) {
        if (selectWhere(entity.getUser().getUsername(), entity.getPicture().getSource().toString()) != null)
            update(entity);
        else {
            String sql = "INSERT INTO imgur.uservote (Username, PictureSource, VoteType) VALUES(?, ?, ?)";
            Connection connection = openConnection();

            try {
                PreparedStatement statement = connection.prepareStatement(sql);
                statement.setString(1, entity.getUser().getUsername());
                statement.setString(2, entity.getPicture().getSource().toString());
                statement.setInt(3, entity.isDownvote() ? 1 : 0);
                statement.executeUpdate();
                connection.close();
            } catch (SQLException e) {
                throw new AdapterException("Error preparing insert statement", e);
            }
        }

        return this;
    }

    @Override
    public List<UserVote> select() {
        String sql = "SELECT * FROM imgur.uservote";
        Connection connection = openConnection();
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            UserVoteRowMapper user_vote_row_mapper = new UserVoteRowMapper();
            List<UserVote> user_votes = user_vote_row_mapper.mapEntities(statement.executeQuery());
            connection.close();
            return user_votes;
        } catch (SQLException e) {
            throw new AdapterException("Error preparing select statement", e);
        }
    }

    @Override
    public UserVote selectWhere(Object... args) {
        final Integer USERNAME       = 0;
        final Integer PICTURE_SOURCE = 1;

        String username = (String) args[USERNAME];
        String source   = (String) args[PICTURE_SOURCE];

        String sql = "SELECT * FROM imgur.uservote WHERE Username = ? AND PictureSource = ?";
        try (Connection connection = openConnection())
        {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, username);
            statement.setString(2, source);
            RowMapper<UserVote> user_vote_row_mapper = new UserVoteRowMapper();
            List<UserVote>  user_vote = user_vote_row_mapper.mapEntities(statement.executeQuery());
            if (user_vote.isEmpty())
                return null;
            else
                return user_vote.get(0);
        } catch (SQLException e) {
            throw new AdapterException("Error preparing select statement", e);
        }
    }

    @Override
    public UserVote update(UserVote entity) {
        UserVote tmp = selectWhere(entity.getUser().getUsername(), entity.getPicture().getSource().toString());

        if (tmp != null) {
            if (tmp.isDownvote() == entity.isDownvote())
            {
                delete(entity);
                return entity;
            }
            else {
                DataAdapter<Picture> picture_data_adapter = new PictureAdapter();
                Picture picture = entity.getPicture();
                if (tmp.isDownvote() && !entity.isDownvote())
                    picture.setUpvotes(picture.getUpvotes() + 1);
                else
                    picture.setUpvotes(picture.getUpvotes() - 1);

                picture_data_adapter.update(picture);
            }
        }


        String sql = "UPDATE imgur.uservote SET VoteType = ? WHERE Username = ? AND PictureSource = ?";

        try (Connection connection = openConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, entity.isDownvote() ? 1 : 0);
            statement.setString(2, entity.getUser().getUsername());
            statement.setString(3, entity.getPicture().getSource().toString());
            return statement.executeUpdate() > 0 ? entity : null;
        } catch (SQLException e) {
            throw new AdapterException("Error preparing update statement", e);
        }
    }

    @Override
    public Boolean delete(UserVote entity) {
        String sql = "DELETE UV FROM imgur.uservote UV WHERE UV.Username = ? AND UV.PictureSource = ?";
        try (Connection connection = openConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, entity.getUser().getUsername());
            statement.setString(2, entity.getPicture().getSource().toString());
            return statement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new AdapterException("Error preparing delete statement", e);
        }
    }

    @Override
    public Boolean delete(Integer id) {
        String sql = "DELETE UV FROM imgur.uservote UV WHERE UV.EntryID = ?";
        return genericDelete(sql, id);
    }
}
