package service;

import model.Picture;

import java.nio.file.Path;

public interface PictureServiceInterface extends ServiceInterface<Picture> {
    String ADD_UPVOTE = "+";
    String SUBTRACT_UPVOTE = "-";

    PictureServiceInterface updateUpvotes(Picture picture, String upvote_type);
    PictureServiceInterface updateUpvotes(Path source, String upvote_type);
    PictureServiceInterface updateViews(Picture picture);
    PictureServiceInterface updateViews(Path source);
}
