package service;

import model.Picture;
import model.User;
import model.UserVote;
import utils.adapters.DataAdapter;
import utils.adapters.UserVoteAdapter;
import utils.factories.UserVoteFactory;

import java.util.List;

public class UserVoteService implements ServiceInterface<UserVote> {
    private DataAdapter<UserVote> user_vote_adapter;

    public UserVoteService() { user_vote_adapter = new UserVoteAdapter(); }

    @Override
    public Boolean exists(UserVote entity) {
        return user_vote_adapter.selectWhere(entity.getUser().getUsername(), entity.getPicture().getSource().toString()) != null;
    }

    @Override
    public Boolean exists(Object... args) {
        final Integer USERNAME       = 0;
        final Integer PICTURE_SOURCE = 1;
        return user_vote_adapter.selectWhere(args[USERNAME], args[PICTURE_SOURCE]) != null;
    }

    @Override
    public UserVote create(Object... args) {
        final Integer USER      = 0;
        final Integer PICTURE   = 1;
        final Integer VOTE_TYPE = 2;

        User user         = (User) args[USER];
        Picture picture   = (Picture) args[PICTURE];
        Boolean vote_type = (Boolean) args[VOTE_TYPE];

        UserVote user_vote = UserVoteFactory.makeUserVote(user, picture, vote_type);
        user_vote_adapter.insertOrUpdate(user_vote);

        return user_vote;
    }

    @Override
    public List<UserVote> read() {
        return user_vote_adapter.select();
    }

    @Override
    public UserVote readOne(Object... args) {
        final Integer USER      = 0;
        final Integer PICTURE   = 1;

        User user         = (User) args[USER];
        Picture picture   = (Picture) args[PICTURE];

        return user_vote_adapter.selectWhere(user.getUsername(), picture.getSource().toString());
    }

    @Override
    public UserVote update(Object... args) {
        final Integer USER      = 0;
        final Integer PICTURE   = 1;
        final Integer VOTE_TYPE = 2;

        User user         = (User) args[USER];
        Picture picture   = (Picture) args[PICTURE];
        Boolean vote_type = (Boolean) args[VOTE_TYPE];

        UserVote user_vote = UserVoteFactory.makeUserVote(user, picture, vote_type);
        user_vote_adapter.update(user_vote);

        return user_vote;
    }

    @Override
    public Boolean delete(UserVote entity) {
        return user_vote_adapter.delete(entity);
    }

    @Override
    public Boolean delete(Integer id) {
        return user_vote_adapter.delete(id);
    }
}
