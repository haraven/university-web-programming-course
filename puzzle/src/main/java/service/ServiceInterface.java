package service;

import model.BaseEntity;

import java.util.List;

public interface ServiceInterface<T extends BaseEntity<Integer>> {
    Boolean exists(T entity);
    Boolean exists(Object... args);
    T create(Object... args);
    List<T> read();
    T readOne(Object... args);
    T update(Object... args);
    Boolean delete(T entity);
    Boolean delete(Integer id);
}
