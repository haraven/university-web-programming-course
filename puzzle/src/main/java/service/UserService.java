package service;

import model.User;
import model.validators.UserValidator;
import utils.adapters.DataAdapter;
import utils.adapters.UserAdapter;
import utils.exceptions.ServiceException;
import utils.exceptions.ValidatorException;
import utils.factories.UserFactory;

import java.util.List;

public class UserService implements ServiceInterface<User> {
    private DataAdapter<User> user_adapter;

    public UserService() {
        user_adapter = new UserAdapter();
    }

    public Boolean exists(User entity) {
        return user_adapter.selectWhere(entity.getUsername(), entity.getPassword()) != null;
    }

    @Override
    public Boolean exists(Object... args) {
        final Integer USERNAME = 0;
        final Integer PASSWORD = 1;
        String username = (String) args[USERNAME];
        String password = args.length > 1 ? (String) args[PASSWORD] : "";
        return user_adapter.selectWhere(username, password) != null;
    }

    public User create(Object... args) {
        final Integer USERNAME = 0;
        final Integer PASSWORD = 1;
        String username = (String) args[USERNAME];
        String password = (String) args[PASSWORD];
        try {
            UserValidator.validate(username, password);
        } catch(ValidatorException e) {
            throw new ServiceException("Error validating user data", e);
        }

        if (exists(username)) throw new ServiceException("Username \'" + username + "\' already exists.");
        User new_user = UserFactory.makeUser(username, password);
        user_adapter.insertOrUpdate(new_user);
        return new_user;
    }

    public List<User> read() {
        return user_adapter.select();
    }

    @Override
    public User readOne(Object... args) {
        final Integer USERNAME = 0;
        final Integer PASSWORD = 1;
        String username = (String) args[USERNAME];
        String password = (String) args[PASSWORD];

        return user_adapter.selectWhere(username, password);
    }

    public User update(Object... args) {
        final Integer USERNAME = 0;
        final Integer PASSWORD = 1;
        String username = (String) args[USERNAME];
        String password = (String) args[PASSWORD];
        return user_adapter.update(UserFactory.makeUser(username, password));
    }

    public Boolean delete(User entity) {
        return user_adapter.delete(entity);
    }

    public Boolean delete(Integer id) {
        return user_adapter.delete(id);
    }
}
