package service;

import model.Picture;
import model.User;
import utils.adapters.DataAdapter;
import utils.adapters.PictureAdapter;
import utils.exceptions.ServiceException;
import utils.factories.PictureFactory;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class PictureService implements PictureServiceInterface {
    private DataAdapter<Picture> picture_adapter;

    public PictureService() {
        picture_adapter = new PictureAdapter();
    }

    @Override
    public PictureServiceInterface updateUpvotes(Picture picture, String upvote_type) {
        Integer upvotes = picture.getUpvotes();
        if (upvote_type.equals(ADD_UPVOTE))
            ++upvotes;
        else
            --upvotes;

        picture.setUpvotes(upvotes);
        picture_adapter.update(picture);

        return this;
    }

    @Override
    public PictureServiceInterface updateUpvotes(Path source, String upvote_type) {
        Picture picture = picture_adapter.selectWhere(source);
        if (picture == null) throw new ServiceException("No picture having the path " + source + " was found.");
        return updateUpvotes(picture, upvote_type);
    }

    @Override
    public PictureServiceInterface updateViews(Picture picture) {
        Integer views = picture.getViews();
        picture.setViews(views + 1);
        picture_adapter.update(picture);

        return this;
    }

    @Override
    public PictureServiceInterface updateViews(Path source) {
        Picture picture = picture_adapter.selectWhere(source);
        if (picture == null) throw new ServiceException("No picture having the path " + source + " was found.");
        return updateViews(picture);
    }

    @Override
    public Boolean exists(Picture entity) {
        return picture_adapter.selectWhere(entity.getSource()) != null;
    }

    @Override
    public Boolean exists(Object... args) {
        final Integer RELATIVE_PATH = 0;
        String relative_path = (String) args[RELATIVE_PATH];
        Path path = Paths.get(relative_path);
        return picture_adapter.selectWhere(path) != null;
    }

    @Override
    public Picture create(Object... args) {
        final Integer TITLE   = 0;
        final Integer PATH    = 1;
        final Integer OP      = 2;
        final Integer UPVOTES = 3;
        final Integer VIEWS   = 4;

        String title         = (String) args[TITLE];
        String path_string   = (String) args[PATH];
        Path path            = Paths.get(path_string);
        User original_poster = (User) args[OP];
        Integer upvotes      = args.length > 3 ? (Integer) args[UPVOTES] : 0;
        Integer views        = args.length > 4 ? (Integer) args[VIEWS] : 0;

        Picture picture = PictureFactory.makePicture(title, path, upvotes, views, original_poster);
        picture_adapter.insertOrUpdate(picture);

        return picture;
    }

    @Override
    public List<Picture> read() {
        return picture_adapter.select();
    }

    @Override
    public Picture readOne(Object... args) {
        final Integer PATH = 0;
        String path_string = (String) args[PATH];
        Path path = Paths.get(path_string);

        return picture_adapter.selectWhere(path);
    }

    @Override
    public Picture update(Object... args) {
        final Integer TITLE   = 0;
        final Integer PATH    = 1;
        final Integer OP      = 2;
        final Integer UPVOTES = 3;
        final Integer VIEWS   = 4;

        String title         = (String) args[TITLE];
        String path_string   = (String) args[PATH];
        Path path            = Paths.get(path_string);
        User original_poster = (User) args[OP];
        Integer upvotes      = args.length > 2 ? (Integer) args[UPVOTES] : 0;
        Integer views        = args.length > 3 ? (Integer) args[VIEWS] : 0;

        Picture picture = PictureFactory.makePicture(title, path, upvotes, views, original_poster);
        return picture_adapter.update(picture);
    }

    @Override
    public Boolean delete(Picture entity) {
        return picture_adapter.delete(entity);
    }

    @Override
    public Boolean delete(Integer id) {
        return picture_adapter.delete(id);
    }
}
