package model;

public class UserVote extends BaseEntity<Integer> {
    private User user;
    private Picture picture;
    private Boolean vote_type;

    public UserVote() {}

    public UserVote(User user, Picture picture, Boolean vote_type) {
        this.user = user;
        this.picture = picture;
        this.vote_type = vote_type;
    }

    public UserVote(Integer integer, User user, Picture picture, Boolean vote_type) {
        super(integer);
        this.user = user;
        this.picture = picture;
        this.vote_type = vote_type;
    }

    public User getUser() {
        return user;
    }

    public UserVote setUser(User user) {
        this.user = user;

        return this;
    }

    public Picture getPicture() {
        return picture;
    }

    public UserVote setPicture(Picture picture) {
        this.picture = picture;

        return this;
    }

    public Boolean isDownvote() {
        return vote_type;
    }

    public UserVote setVoteType(Boolean vote_type) {
        this.vote_type = vote_type;

        return this;
    }
}
