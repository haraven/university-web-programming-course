package model;

import java.nio.file.Path;
import java.nio.file.Paths;

public class Picture extends BaseEntity<Integer> {
    private String title;
    private Path source;
    private Integer upvotes;
    private Integer views;
    private User OP;

    public Picture() {}

    public Picture(String title, String source_string, User OP) {
        this(title, Paths.get(source_string), OP);
    }

    public Picture(Integer id, String title, String source_string, User OP) {
        this(id, title, Paths.get(source_string), OP);
    }

    public Picture(String title, String source_string, Integer upvotes, Integer views, User OP) {
        this(title, Paths.get(source_string), upvotes, views, OP);
    }

    public Picture(Integer id, String title, String source_string, Integer upvotes, Integer views, User OP) {
        this(id, title, Paths.get(source_string), upvotes, views, OP);
    }

    public Picture(String title, Path source, User OP) {
        this.title  = title;
        this.source = source;
        upvotes     = 0;
        views       = 0;
        this.OP     = OP;
    }

    public Picture(Integer id, String title, Path source, User OP) {
        super(id);
        this.source = source;
        this.title  = title;
        upvotes     = 0;
        views       = 0;
        this.OP     = OP;

    }

    public Picture(Integer id, String title, Path source, Integer upvotes, Integer views, User OP) {
        super(id);
        this.title   = title;
        this.source  = source;
        this.upvotes = upvotes;
        this.views   = views;
        this.OP      = OP;
    }

    public Picture(String title, Path source, Integer upvotes, Integer views, User OP) {
        this.title   = title;
        this.source  = source;
        this.upvotes = upvotes;
        this.views   = views;
        this.OP      = OP;
    }

    public String getTitle() {
        return title;
    }

    public Picture setTitle(String title) {
        this.title = title;

        return this;
    }

    public Path getSource() {
        return source;
    }

    public Picture setSource(Path source) {
        this.source = source;

        return this;
    }

    public Picture setSource(String source_string) {
        this.source = Paths.get(source_string);
        return this;
    }

    public Integer getUpvotes() {
        return upvotes;
    }

    public Picture setUpvotes(Integer upvotes) {
        this.upvotes = upvotes;

        return this;
    }

    public Integer getViews() {
        return views;
    }

    public Picture setViews(Integer views) {
        this.views = views;

        return this;
    }

    public User getOP() {
        return OP;
    }

    public Picture setOP(User OP) {
        this.OP = OP;

        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Picture picture = (Picture) o;

        return source.equals(picture.source);

    }

    @Override
    public int hashCode() {
        return source.hashCode();
    }

    @Override
    public String toString() {
        return "Title: " + title + ".\n" + upvotes + " upvotes; " + views + " views.\nPosted by: " + OP.getUsername();
    }
}
