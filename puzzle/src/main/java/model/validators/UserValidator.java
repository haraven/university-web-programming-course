package model.validators;

import model.User;
import utils.exceptions.ValidatorException;

public class UserValidator {
    public static void validate(User entity) throws ValidatorException {
        StringBuilder sb = new StringBuilder("");
        if (entity == null) throw new ValidatorException("User cannot be null.");
        if (entity.getUsername().equals("")) sb.append("Username cannot be null. ");
        if (entity.getPassword().equals("")) sb.append("Password cannot be null.");

        String res = sb.toString();
        if (!res.equals("")) throw new ValidatorException(res);
    }

    public static void validate(Object... args) throws ValidatorException {
        if (args.length == 0) throw new ValidatorException("Nothing to validate.");
        final Integer USERNAME = 0;
        final Integer PASSWORD = 1;

        String username = (String) args[USERNAME];
        String password = (String) args[PASSWORD];
        StringBuilder sb = new StringBuilder("");
        if (username.equals("")) sb.append("Username cannot be null. ");
        if (password.equals("")) sb.append("Password cannot be null.");

        String res = sb.toString();
        if (!res.equals("")) throw new ValidatorException(res);
    }
}
