package model;

import java.io.Serializable;

public class BaseEntity<ID extends Serializable> {
    private ID entry_id;

    BaseEntity() {}

    BaseEntity(ID id) {
        this.entry_id = id;
    }

    public ID getEntryID() {
        return entry_id;
    }

    public BaseEntity<ID> setEntryID(ID id) {
        entry_id = id;

        return this;
    }
}
