package servlet;

import model.User;
import org.apache.commons.io.FileUtils;
import service.PictureService;
import service.PictureServiceInterface;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet("/upload")
@MultipartConfig
public class UploadServlet extends HttpServlet {
    private PictureServiceInterface picture_service;

    @Override
    public void init(ServletConfig config) throws ServletException {
        picture_service = new PictureService();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            final String UPLOAD_DIR = Paths.get("G:/University/Web programming/pictures").toString();
            List<Part> file_parts = request.getParts()
                    .stream()
                    .filter(part -> part.getName().contains("file"))
                    .collect(Collectors.toList());

            User user = (User) request.getSession().getAttribute("user");
            for (Part file_part : file_parts) {
                String file_name = file_part.getSubmittedFileName();
                if (file_name.equals(""))
                    request.getRequestDispatcher("/home").forward(request, response);

                String extension = file_name.substring(file_name.lastIndexOf('.'));
                InputStream file_content = file_part.getInputStream();
                File tmp = File.createTempFile("img_", extension, new File(UPLOAD_DIR));
                FileUtils.copyInputStreamToFile(file_content, tmp);
                String tmp_name = tmp.getName();
                picture_service.create(tmp_name.substring(tmp_name.indexOf('_') + 1), tmp.getPath(), user);
            }
            request.setAttribute("message", "Picture uploaded successfully!");
        }
        catch(Exception e) {
            e.printStackTrace();
            request.setAttribute("message", "Error uploading picture: " + e.getMessage());
        }

        request.getRequestDispatcher("/home").forward(request, response);
    }
}
