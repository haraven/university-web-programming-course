package servlet;

import model.User;
import model.validators.UserValidator;
import service.UserService;
import service.ServiceInterface;
import utils.exceptions.ServiceException;
import utils.exceptions.ValidatorException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/register")
public class RegisterServlet extends HttpServlet{
    private ServiceInterface<User> user_service;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        try {
            User user = user_service.create(username, password);
            request.getSession().setAttribute("user", user);
        } catch(ServiceException e) {
            e.printStackTrace();
            Throwable cause = e.getCause();
            request.setAttribute("error", cause != null ? cause.getMessage() : e.getMessage());
            request.getRequestDispatcher("/login.jsp").forward(request, response);
        }

        response.sendRedirect(request.getContextPath() + "/home");
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        user_service = new UserService();
    }
}
