package servlet;

import model.Picture;
import model.User;
import model.UserVote;
import service.PictureService;
import service.PictureServiceInterface;
import service.ServiceInterface;
import service.UserVoteService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLConnection;
import java.nio.file.Files;
import java.util.List;
import java.util.Optional;

@WebServlet("/pictures/*")
public class PictureServlet extends HttpServlet {
    private PictureServiceInterface picture_service;
    private ServiceInterface<UserVote> user_vote_service;


    @Override
    public void init(ServletConfig config) throws ServletException {
        picture_service   = new PictureService();
        user_vote_service = new UserVoteService();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String request_path = request.getPathInfo();
            String image_name = request_path.substring(request_path.lastIndexOf('/') + 1);
            List<Picture> pics = picture_service.read();
            Optional<Picture> pic = pics.stream()
                    .filter(picture -> picture.getSource().toString().contains(image_name))
                    .findFirst();
            if (!pic.isPresent())
                response.sendError(HttpServletResponse.SC_NOT_FOUND, "Image " + image_name + " was not found.");
            else {
                byte[] content = Files.readAllBytes(pic.get().getSource());
                response.setContentType(URLConnection.guessContentTypeFromName(image_name));
                response.setContentLength(content.length);
                response.getOutputStream().write(content);
            }
        } catch (Exception e) {
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String request_path = request.getPathInfo();
            String image_name = request_path.substring(request_path.lastIndexOf('/') + 1);
            List<Picture> pics = picture_service.read();
            Optional<Picture> pic = pics.stream()
                    .filter(picture -> picture.getSource().toString().contains(image_name))
                    .findFirst();
            if (!pic.isPresent()) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND, "Image " + image_name + " was not found.");
                return;
            }

            Picture picture = pic.get();
            String action = request.getParameter("btn");
            switch(action)
            {
                case "UPVOTE":
                    picture_service.updateUpvotes(picture, PictureServiceInterface.ADD_UPVOTE);
                    user_vote_service.create(request.getSession().getAttribute("user"), picture, false);
                    break;
                case "DOWNVOTE":
                    picture_service.updateUpvotes(picture, PictureServiceInterface.SUBTRACT_UPVOTE);
                    user_vote_service.create(request.getSession().getAttribute("user"), picture, true);
                    break;
                case "DELETE":
                    User user = (User) request.getSession().getAttribute("user");
                    if (!user.equals(picture.getOP())) {
                        response.sendError(HttpServletResponse.SC_FORBIDDEN);
                        return;
                    }
                    else
                        picture_service.delete(picture);
                    break;
                default:
                    break;

            }
            request.setAttribute("picture", picture);
            String source = picture.getSource().toString();
            request.getRequestDispatcher("/gallery/" + source.substring(source.lastIndexOf('\\') + 1)).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }

    }
}
