package servlet;

import model.Picture;
import service.PictureService;
import service.PictureServiceInterface;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet("/home")
public class HomeServlet extends HttpServlet {
    private PictureServiceInterface picture_service;
    private Integer result_count = 5;

    @Override
    public void init(ServletConfig config) throws ServletException {
        picture_service = new PictureService();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("resultcount", result_count);
        List<Picture> pictures = picture_service.read();
        List<String> picture_links = pictures.stream()
                .sorted((picture1, picture2) -> Integer.compare(picture2.getUpvotes(), picture1.getUpvotes()))
                .map(picture -> {
                    String source = picture.getSource().toString();
                    return source.substring(source.lastIndexOf('\\') + 1);
                })
                .limit(result_count)
                .collect(Collectors.toList());
        request.setAttribute("pics", picture_links);
        request.getRequestDispatcher("/home.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getParameter("resultcount") != null) {
            result_count = Integer.parseInt(request.getParameter("resultcount"));
            if (result_count < 1)
                result_count = 1;
        }

        doGet(request, response);
    }
}
