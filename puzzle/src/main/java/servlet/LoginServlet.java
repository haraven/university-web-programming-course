package servlet;

import model.User;
import model.validators.UserValidator;
import service.UserService;
import service.ServiceInterface;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    private ServiceInterface<User> user_service;

    @Override
    public void init(final ServletConfig config) throws ServletException {
        user_service = new UserService();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            if (!request.getRequestURI().contains("login")) // any request not from login.jsp is a logout request
            {
                HttpSession session = request.getSession(false);
                if (session != null)
                    session.invalidate();
                request.getRequestDispatcher("/login.jsp").forward(request, response);
            }
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            UserValidator.validate(username, password);

            if (user_service.exists(username, password)) {
                User user = user_service.readOne(username, password);
                if (user != null) {
                    request.removeAttribute("username");
                    request.removeAttribute("password");
                    request.getSession().setAttribute("user", user);
                    response.sendRedirect(request.getContextPath() + "/home");
                } else {
                    request.setAttribute("error", "Wrong username or password");
                    doGet(request, response);
                }
            } else {
                request.setAttribute("error", "Unknown login, try again");
                doGet(request, response);
            }
        } catch (Exception e) {
            e.printStackTrace();
            request.setAttribute("error", e.getMessage());
            doGet(request, response);
        }
    }
}
