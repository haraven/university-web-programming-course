package servlet.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("/*")
public class RedirectFilter implements Filter {
    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        HttpSession session = request.getSession(false);
        String request_uri = request.getRequestURI();
        String login_uri = request.getContextPath() + "/login";
        String register_uri = request.getContextPath() + "/register";

        boolean logged_in = session != null && session.getAttribute("user") != null;
        boolean login_request = request_uri.equals(login_uri) || request_uri.equals(register_uri) || request_uri.contains(".css") || request_uri.contains(".js");

        if (logged_in)
            if (request_uri.equals(login_uri)) {
                response.sendRedirect(request.getContextPath() + "/home");
            } else
                chain.doFilter(request, response);
        else if (login_request)
            chain.doFilter(request, response);
        else
            response.sendRedirect(login_uri);
    }
}
