package servlet;

import model.Picture;
import model.User;
import model.UserVote;
import service.PictureService;
import service.PictureServiceInterface;
import service.ServiceInterface;
import service.UserVoteService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@WebServlet("/gallery/*")
public class GalleryServlet extends HttpServlet {
    private PictureServiceInterface picture_service;
    private ServiceInterface<UserVote> user_vote_service;

    @Override
    public void init(ServletConfig config) throws ServletException {
        picture_service   = new PictureService();
        user_vote_service = new UserVoteService();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("user_votes", user_vote_service.read());
        String request_path = request.getPathInfo();
        String image_name = request_path.substring(request_path.lastIndexOf('/') + 1);
        List<Picture> pics = picture_service.read();
        Optional<Picture> pic = pics.stream()
                .filter(picture -> picture.getSource().toString().contains(image_name))
                .findFirst();

        if (!pic.isPresent())
            response.sendError(HttpServletResponse.SC_NOT_FOUND, "Image " + image_name + " was not found.");
        else {
            Picture picture = pic.get();
            picture_service.updateViews(picture);
            request.setAttribute("picture", picture);
            request.getRequestDispatcher("/gallery.jsp").forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
