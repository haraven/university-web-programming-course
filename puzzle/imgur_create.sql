-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.11-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for imgur
CREATE DATABASE IF NOT EXISTS `imgur` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `imgur`;


-- Dumping structure for table imgur.picture
CREATE TABLE IF NOT EXISTS `picture` (
  `EntryID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Title` varchar(30) DEFAULT NULL,
  `Source` varchar(255) NOT NULL,
  `Upvotes` int(11) NOT NULL DEFAULT '0',
  `Views` int(10) unsigned NOT NULL DEFAULT '0',
  `UserEntryID` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`EntryID`),
  KEY `FK_PictureUserEntryID` (`UserEntryID`),
  CONSTRAINT `FK_PictureUserEntryID` FOREIGN KEY (`UserEntryID`) REFERENCES `user` (`EntryID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table imgur.picture: ~0 rows (approximately)
/*!40000 ALTER TABLE `picture` DISABLE KEYS */;
/*!40000 ALTER TABLE `picture` ENABLE KEYS */;


-- Dumping structure for table imgur.user
CREATE TABLE IF NOT EXISTS `user` (
  `EntryID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Username` varchar(30) NOT NULL DEFAULT 'admin',
  `Password` varchar(30) NOT NULL DEFAULT 'admin',
  `Email` varchar(50) NOT NULL DEFAULT '""',
  PRIMARY KEY (`EntryID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Dumping data for table imgur.user: ~2 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;


-- Dumping structure for table imgur.uservote
CREATE TABLE IF NOT EXISTS `uservote` (
  `EntryID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Username` varchar(30) NOT NULL,
  `PictureSource` varchar(255) NOT NULL,
  `VoteType` int(1) unsigned NOT NULL DEFAULT '0' COMMENT '0 = upvote, 1 = downvote',
  PRIMARY KEY (`EntryID`),
  KEY `FK_UserVotesUserEntryID` (`Username`),
  KEY `FK_UserVotesPictureEntryID` (`PictureSource`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table imgur.uservote: ~0 rows (approximately)
/*!40000 ALTER TABLE `uservote` DISABLE KEYS */;
/*!40000 ALTER TABLE `uservote` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
