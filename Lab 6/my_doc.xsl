<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <head>
                <link rel="stylesheet" type="text/css" href="themes/global.css" />
            </head>
            <body>
                <xsl:apply-templates/>
            </body>
        </html>
    </xsl:template>

<xsl:template match="//*[not(*)]">
    <xsl:choose>
        <xsl:when test="contains(current(), '.png') or contains(current(), '.jpg') or contains(current(), '.jpeg') or contains(current(), '.gif') or contains(current(), '.tiff') or contains(current(), '.bmp')">
            <xsl:element name="img">
                <xsl:attribute name="src">
                    <xsl:value-of select="current()"/>
                </xsl:attribute>
                <xsl:attribute name="alt">
                    <xsl:value-of select="current()"/> could not be displayed.
                </xsl:attribute>
            </xsl:element>
            <br />
        </xsl:when>
        <xsl:when test="(starts-with(current(), 'http://') or starts-with(current(), 'https://')) and not(contains(current(), ' '))">
            <xsl:element name="a">
                <xsl:attribute name="href">
                    <xsl:value-of select="current()"/>
                </xsl:attribute>
                <xsl:value-of select="current()"/>
            </xsl:element>
            is a link
            <br />
        </xsl:when>
        <xsl:otherwise>
            <span><xsl:value-of select="current()"/> is just text.</span>
            <br />
        </xsl:otherwise>
    </xsl:choose>

</xsl:template>
</xsl:stylesheet>