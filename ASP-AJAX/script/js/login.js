$(document).ready(function()
{
    $("#btn-login").click(OnLoginClicked);
    $("#btn-register").click(OnRegisterClicked);
});

function OnLoginClicked() 
{
    event.preventDefault();

    var username = $("#username").val();
    var password = $("#password").val();
    var errors = ValidateInput(username, password);
    if (errors != "")
    {
        $("#error").fadeIn(1000, function()
        {
            $("#error").html("<div id=\"error\">" + errors + "</div>").fadeOut(4000);
        });

        return;
    }

    $.ajax
    ({
        url: 'login.aspx/OnLogin',
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        data:
        '{' +
          "username: \"" + username + "\"," +
          "password: \"" + password + '"' +
        '}',
        dataType: "json",
        success: function(data) 
        {
            var res = data.d;
            if (res.indexOf("OK") < 0)
            {
                $("#error").html("<div id=\"error\">" + res + "</div>").fadeOut(4000);
            }
            else
            {
                $.session.set('username', username);
                window.location.href = "home.aspx";
            }
        },
        error: function (xmlHttpRequest, textStatus, errorThrown) 
        {
            $("#error").html("<div id=\"error\">" + xmlHttpRequest.responseText + "</div>").fadeOut(4000);
        }
    });
}

function OnRegisterClicked()
{
    event.preventDefault();

    var username = $("#username").val();
    var password = $("#password").val();
    var errors = ValidateInput(username, password);
    if (errors != "") {
        $("#error").fadeIn(1000, function () {
            $("#error").html("<div id=\"error\">" + errors + "</div>").fadeOut(4000);
        });

        return;
    }

    $.ajax
    ({
        url: 'login.aspx/OnRegister',
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        data:
        '{' +
          "username: \"" + username + "\"," +
          "password: \"" + password + '"' +
        '}',
        dataType: "json",
        success: function (data) {
            var res = data.d;
            if (res.indexOf("done") < 0) {
                $("#error").html("<div id=\"error\">" + res + "</div>").fadeOut(4000);
            }
            else {
                $.session.set('username', username);
                window.location.href = "home.aspx";
            }
        },
        error: function (xmlHttpRequest, textStatus, errorThrown) {
            $("#error").html("<div id=\"error\">" + xmlHttpRequest.responseText + "</div>").fadeOut(4000);
        }
    });
}

function ValidateInput(username, password)
{
    var errors = "";
    if (username == "" || username === undefined)
        errors += "Username cannot be null.";

    if (errors != "")
        errors += "<br>";

    if (password == "" || password === undefined)
        errors += "Password cannot be null."
    
    return errors;
}