﻿using MySql.Data.MySqlClient;
using System;
using System.Data.SqlClient;
using System.Web.Script.Services;

public partial class login : System.Web.UI.Page
{
    [System.Web.Services.WebMethod]
    public static string OnLogin(string username, string password)
    {
        int entry_id = -1;
        try
        {
            using (var conn = new MySqlConnection("server=127.0.0.1;port=3306;uid=root;" +
                "pwd=RESILIENCEWILLFIXIT;database=wdb;"))
            {
                string sql = "SELECT * FROM wdb.user WHERE Username = @username AND Password = @password";
                MySqlCommand select = new MySqlCommand(sql, conn);
                select.Parameters.AddWithValue("@username", username);
                select.Parameters.AddWithValue("@password", password);

                conn.Open();

                var reader = select.ExecuteReader();

                if (reader.HasRows)
                {
                    reader.Read();

                    entry_id = Convert.ToInt32(reader["EntryID"].ToString());
                }
                else
                {
                    return "Invalid username or password";
                }

                reader.Close();
                conn.Close();
            }
        }
        catch (Exception)
        {
            return "Internal error";
        }

        return "OK";
    }

    [System.Web.Services.WebMethod]
    public static string OnRegister(string username, string password)
    {
        try
        {
            using (var conn = new MySqlConnection("server=127.0.0.1;port=3306;uid=root;" +
                "pwd=RESILIENCEWILLFIXIT;database=wdb;"))
            {
                conn.Open();
                string sql = "SELECT * FROM wdb.user WHERE Username = @username";
                MySqlCommand exists = new MySqlCommand(sql, conn);
                exists.Parameters.AddWithValue("@username", username);
                var reader = exists.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Close();
                    return "username already exists";
                }

                reader.Close();
                sql = "INSERT INTO wdb.user (Username, Password) VALUES (@username, @password)";
                MySqlCommand select = new MySqlCommand(sql, conn);
                select.Parameters.AddWithValue("@username", username);
                select.Parameters.AddWithValue("@password", password);
                select.ExecuteNonQuery();
                conn.Close();
            }
        }
        catch (Exception e)
        {
            return e.Message;
        }


        return "done";
    }
}