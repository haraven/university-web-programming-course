﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>LOGIN</title>
    <link rel="stylesheet" href="themes/css/ui.css" />
    <script src="script/js/jquery-2.2.3.min.js"></script>
    <script src="script/js/jquery.session.js"></script>
    <script src="script/js/login.js"></script>
</head>
<body>
    <div class="container">
        <h1 class="welcome-msg">
            LOG IN
        </h1>
        <form id="login-form">
            <div id="error">
            </div>
            <div id="username-group">
                <span>
                    Username:
                </span>
                <br />
                <input type="text" id="username" />
            </div>
            <br />
            <div id="password-group">
                <span>
                    Password:
                </span>
                <br />
                <input type="password" id="password" />
            </div>
            <br />
            <button type="submit" id="btn-login">LOG IN</button>
            <button type="submit" id="btn-register">REGISTER</button>
        </form>
    </div>
</body>
</html>
