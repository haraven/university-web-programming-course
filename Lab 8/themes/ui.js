const ROW_FORMAT = `
<tr>
    <td class="btn">
        <button class="delete" type="button" onclick="javascript:OnDeleteRow(this)">DEL</button>
    </td>
    <td class="text-cell">
        <input type="text" style="display:table-cell; width:100%; height: 100%" onchange="OnInputAdded(this)">
    </td>
    <td class="text-cell">
        <input type="text" style="display:table-cell; width:100%; height: 100%" onchange="OnInputAdded(this)">
    </td>
    <td class="text-cell">
        <input type="text" style="display:table-cell; width:100%; height: 100%" onchange="OnInputAdded(this)">
    </td>
    <td class="text-cell">
        <input type="text" style="display:table-cell; width:100%; height: 100%" onchange="OnInputAdded(this)">
    </td>
    <td class="text-cell">
        <input type="text" style="display:table-cell; width:100%; height: 100%" onchange="OnInputAdded(this)">
    </td>
    <td class="btn">
        <button class="add" type="button" onclick="javascript:OnAddRow(this)">ADD</button>
    </td>
</tr>`;

function OnDeleteRow(cell)
{
    var row = $(cell).closest("tr");
    $(row).fadeOut(500, function()
    {
        row.replaceWith(row.prev());
    });
}

function OnAddRow(prev_row)
{
    var row = $(prev_row).closest("tr");
    var new_row = $(ROW_FORMAT);
    new_row.find(".add").click(OnAddRow);
    new_row.find(".delete").click(OnDeleteRow);
    var cols = new_row.find("td");
    new_row.insertAfter(row);
}

function OnInputAdded(input_box)
{
    var parent = input_box.closest("tr");
    var children = $(parent).find(".text-cell");
    var filled_text_count = 0;
    jQuery.each(children, function(i, child)
        {
            var input = $(child).find("input");
            if (input.val() !== "")
                ++filled_text_count;
        });
    if (filled_text_count == children.length)
    {
        jQuery.each(children, function(i, child)
        {
            var input = $(child).find("input");
            var val = $(child).find("input").val();
            input.replaceWith(val);
        });
    }
}