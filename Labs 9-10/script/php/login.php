<?php
session_start();
require_once 'dbconfig.php';

if (!isset($_SESSION['username']))
{
    if(isset($_POST['username']) && isset($_POST['password']))
    {
        $username = trim($_POST['username']);
        $password = trim($_POST['password']);
        
        $db_manager = new DBManager;
        $db_con = $db_manager->TryConnect($username, $password);
        if (!is_string($db_con))
        {
            $_SESSION['username'] = $username;
            $_SESSION['password'] = $password;
            echo 'OK';
        }
        else
        {
            echo $db_con;
        }
    }
    else
    {
        echo 'No username/password received';
    }
}
else
{
    echo 'Connection already open';
}
?>