<?php
if (session_status() == PHP_SESSION_NONE) 
{
    session_start();
}

class DBManager
{
    public $db_host = "localhost:3306";
    public $db_name = "wdb";

    function TryConnect($username, $password)
    {
        try
        {
            $db_manager = new PDO("mysql:host={$this->db_host};dbname={$this->db_name}", $username, $password);
            $db_manager->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $db_manager;
        }
        catch(PDOException $e)
        {
            return $e->getMessage();
        }
    }

    function GetDBName()
    {
        return $this->db_name;
    }

    function Create()
    {
        try
        {
            $name      = $_POST['name'];
            $displayid = $_POST['displayid'];
            $quality   = $_POST['quality'];
            $buycount  = $_POST['buycount'];
            $buyprice  = $_POST['buyprice'];
            $sellprice = $_POST['sellprice'];

            $username = $_SESSION['username'];
            $password = $_SESSION['password'];
            $db_connection = new PDO("mysql:host={$this->db_host};dbname={$this->db_name}", $username, $password);
            $db_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $statement = $db_connection->prepare("INSERT INTO item (name, displayid, quality, buycount, buyprice, sellprice) VALUES (:name, :displayid, :quality, :buycount, :buyprice, :sellprice)");
            $statement->execute(array
            (
                "name"      => $name,
                "displayid" => $displayid,
                "quality"   => $quality,
                "buycount"  => $buycount,
                "buyprice"  => $buyprice,
                "sellprice" => $sellprice
            ));

            return "done";
        }
        catch (PDOException $e)
        {
            return $e->getMessage();
        }
    }

    function Select()
    {
        $username = $_SESSION['username'];
        $password = $_SESSION['password'];
        $db_connection = new PDO("mysql:host={$this->db_host};dbname={$this->db_name}", $username, $password);
        $db_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "SELECT * FROM item";
        $html = "<table id=\"my-table\">
        <thead><tr>
        <th>entry</th>
        <th>name</th>
        <th>displayid</th>
        <th>quality</th>
        <th>buycount</th>
        <th>buyprice</th>
        <th>sellprice</th>
        </tr></thead>
        <tbody>";
        foreach($db_connection->query($sql) as $row)
        {
            $html .= "<tr>
            <td class=\"text-cell\">";
            $html .= "<a id=\"entry-link{$row['entry']}\" onclick='setEntry(this)' href=\"update.php\">{$row['entry']}</a>";
            $html .= "</td><td class=\"text-cell\">";
            $html .= "{$row['name']}";
            $html .= "</td><td class=\"text-cell\">";
            $html .= "{$row['displayid']}";
            $html .= "</td><td class=\"text-cell\">";
            $html .= "{$row['quality']}";
            $html .= "</td><td class=\"text-cell\">";
            $html .= "{$row['buycount']}";
            $html .= "</td><td class=\"text-cell\">";
            $html .= "{$row['buyprice']}";
            $html .= "</td><td class=\"text-cell\">";
            $html .= "{$row['sellprice']}";
            $html .= "</td></tr>";
        }
        $html .= "</tbody></table>";

        return $html;
    }

    function SelectWhereID()
    {
        $username = $_SESSION['username'];
        $password = $_SESSION['password'];
        $entry    = $_GET['entry'];
        $db_connection = new PDO("mysql:host={$this->db_host};dbname={$this->db_name}", $username, $password);
        $db_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $statement = $db_connection->prepare("SELECT * FROM item WHERE entry = :entry");
        $statement->bindParam(':entry', $entry, PDO::PARAM_INT);
        $statement->execute();
        $my_string = "";
        $row = $statement->fetchObject();
        if ($row != FALSE)
        {    
            $my_string = $row->entry . "|" . $row->name . "|" . $row->displayid . "|" . $row->quality . "|" . $row->buycount . "|" . $row->buyprice . "|" . $row->sellprice;
            return $my_string;        
        }
        else
            return "no result for entry " . $entry;
    }

    function Update()
    {
        try
        {
            $entry     = $_POST['entry'];
            $name      = $_POST['name'];
            $displayid = $_POST['displayid'];
            $quality   = $_POST['quality'];
            $buycount  = $_POST['buycount'];
            $buyprice  = $_POST['buyprice'];
            $sellprice = $_POST['sellprice'];

            $username = $_SESSION['username'];
            $password = $_SESSION['password'];
            $db_connection = new PDO("mysql:host={$this->db_host};dbname={$this->db_name}", $username, $password);
            $db_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $statement = $db_connection->prepare("UPDATE item SET name = :name, displayid = :displayid, quality = :quality, buycount = :buycount, buyprice = :buyprice, sellprice = :sellprice WHERE entry = :entry");

            $statement->execute(array
            (
                "name"      => $name,
                "displayid" => $displayid,
                "quality"   => $quality,
                "buycount"  => $buycount,
                "buyprice"  => $buyprice,
                "sellprice" => $sellprice,
                "entry"     => $entry,
            ));

            if ($statement->rowCount() > 0)
                return "done";
            else
                return "no entry found for id " + $entry;
        }
        catch (PDOException $e)
        {
            return $e->getMessage();
        }
    }

    function Delete()
    {
        try
        {
            $headers = getallheaders();
            $entry   = $headers['entry'];

            $username = $_SESSION['username'];
            $password = $_SESSION['password'];
            $db_connection = new PDO("mysql:host={$this->db_host};dbname={$this->db_name}", $username, $password);
            $db_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $statement = $db_connection->prepare("DELETE from item WHERE entry = :entry");
            $statement->execute(array
            (
                "entry" => $entry
            ));

            if ($statement->rowCount() > 0)
                return "done";
            else
                return "no entry found for id " . $entry;
        }
        catch (PDOException $e)
        {
            return $e->getMessage();
        }
    }
}

if ($_SERVER['REQUEST_METHOD'] == 'GET')
{
    if (!isset($_SESSION['username']))
        echo 'Session expired';
    else if (isset($_GET['op']))
    {
        $db_manager = new DBManager;
        if ($_GET['op'] == 'select')
        { 
            echo $db_manager->Select();
        }
        else if ($_GET['op'] = 'select_where')
            echo $db_manager->SelectWhereID();
    }
}
else if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
    if (isset($_SESSION['username']))
    {
        $errors = "";
        if (!isset($_POST['name']))
            $errors .= "<p>Name not sent</p><br />";
        if (!isset($_POST['displayid']))
            $errors .= "<p>Displayid not sent</p><br />";
        if (!isset($_POST['quality']))
            $errors .= "<p>Quality not sent</p><br />";
        if (!isset($_POST['buycount']))
            $errors .= "<p>Buycount not sent</p><br />";
        if (!isset($_POST['buyprice']))
            $errors .= "<p>buyprice not sent</p><br />";
        if (!isset($_POST['sellprice']))
            $errors .= "<p>Sellprice not sent</p><br />";
        if ($errors != "")
            echo $errors;
        else
        {
            $db_manager = new DBManager;
            if (isset($_POST['entry']))
                echo $db_manager->Update();
            else 
                echo $db_manager->Create();
        }
    }
}
else if ($_SERVER['REQUEST_METHOD'] == 'DELETE')
{
    $db_manager = new DBManager;
    echo $db_manager->Delete();
}
    
?>