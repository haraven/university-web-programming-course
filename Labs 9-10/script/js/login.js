$('document').ready(function()
{
    $("#login-form").submit(function(event)
    {
        event.preventDefault();

        xmlhttp = GetXMLHTTPObject();
        if (xmlhttp == null)
        {
            alert ("Browser doesn't support XMLHTTP.");
            return;
        }

        var username = $("#username").val();
        var password = $("#password").val();
        var errors = ValidateInput(username, password);
        if (errors != "")
        {
            $("#error").fadeIn(1000, function()
            {
                $("#error").html("<div id=\"error\">" + errors + "</div>").fadeOut(4000);
            });

            return;
        }

        var url = "script/php/login.php";
        xmlhttp.onreadystatechange = StateChanged;
        xmlhttp.open("POST", url, true);
        xmlhttp.send(GetFormData());
    });
});

function StateChanged()
{
    if (xmlhttp.readyState == 4)
        if (xmlhttp.responseText != "OK" && xmlhttp.responseText != "")
            $("#error").fadeIn(1000, function() 
            {
                $("#error").html("<div id=\"error\">" + xmlhttp.responseText + "</div>").fadeOut(4000);
            });
        else
        {
            $("body").html("<p>Login successful. Redirecting you to the homepage...</p>");
            window.location.href = "home.php";
        }
}

function ValidateInput(username, password)
{
    var errors = "";
    if (username == "" || username === undefined)
        errors += "Username cannot be null.";

    if (errors != "")
        errors += "<br>";

    if (password == "" || password === undefined)
        errors += "Password cannot be null."
    
    return errors;
}