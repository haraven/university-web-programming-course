function GetFormData() 
{
    var username = $("#username").val();
    var password = $("#password").val();

    var form_data = new FormData();
    form_data.append("username", username);
    form_data.append("password", password);

    return form_data;
}

function GetXMLHTTPObject()
{
    if (window.XMLHttpRequest)
        return new XMLHttpRequest();
    if (window.ActiveXObject)
        return new ActiveXObject("Microsoft.XMLHTTP");
    return null;
}

function CheckSessionStatus()
{
    xmlhttp = GetXMLHTTPObject();
    if (xmlhttp == null)
    {
        alert ("Browser doesn't support XMLHTTP.");
        return;
    }

    var url = "script/php/login.php";
    xmlhttp.onreadystatechange = function()
    {
        if (xmlhttp.readyState == 4)
            if (xmlhttp.responseText == "Connection already open")
            {
                if (window.location.href != "http://wdbtest.com/home.php")
                    window.location.href = "home.php";
            }
            else if (window.location.href.indexOf("index.php") == -1)
            {
                $("body").html("<p>You need to log in first. Redirecting you to login page...</p>")
                window.location.href = "index.php";
            }
    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}

function GoBack()
{
    $("body").html("<p>Redirecting you to the homepage...</p>");
    window.location.href ="home.php";
}

function setEntry(my_val) {
    $.session.set("entry", my_val.innerHTML);
}