$('document').ready(function()
{
    $(".input-form").submit(function(event)
    {
        var name = $("#name").val();
        if (name == "")
        {
            $("#error").fadeIn(1000, function()
            {
                $("#error").html("<div id=\"error\">Name cannot be null.</div>").fadeOut(4000);
            });

            return;
        }
        
        event.preventDefault();

        xmlhttp = GetXMLHTTPObject();
        if (xmlhttp == null)
        {
            alert ("Browser doesn't support XMLHTTP.");
            return;
        }

        var url = "script/php/dbconfig.php";
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4)
            {
                if (xmlhttp.responseText != "done")
                    $("#error").fadeIn(1000, function()
                    {
                        $("#error").html("<div id=\"error\">" + xmlhttp.responseText + "</div>").fadeOut(4000);
                    });
                else
                    $("#error").text("success!").fadeOut(4000);
            }
        };
        xmlhttp.open("POST", url, true);
        xmlhttp.send(GetInputFormData());
    });
});

function GetInputFormData()
{
    var name      = $("#name").val();
    var displayid = $("#displayid").val();
    var quality   = $("#quality").val();
    var buycount  = $("#buycount").val();
    var buyprice  = $("#buyprice").val();
    var sellprice = $("#sellprice").val();

    var form_data = new FormData();
    form_data.append("name", name);
    form_data.append("displayid", displayid);
    form_data.append("quality", quality);
    form_data.append("buycount", buycount);
    form_data.append("buyprice", buyprice);
    form_data.append("sellprice", sellprice);

    return form_data;
}