$("document").ready(function()
{
    $("#btn-logout").click(function() 
    {
        xmlhttp = GetXMLHTTPObject();
        if (xmlhttp == null)
        {
            alert ("Browser doesn't support XMLHTTP.");
            return;
        }

        var url = "script/php/logout.php";
        xmlhttp.onreadystatechange = StatusChanged;

        xmlhttp.open("POST", url, true);
        xmlhttp.send();
    });
});

function StatusChanged()
{
    if (xmlhttp.readyState == 4)
        window.location.href = "index.php";
}