<!DOCTYPE html>
<html>
<head>
    <title>Delete an existing item</title>
    <script src="script/js/jquery-2.2.3.min.js"></script>
    <script src="script/js/ui.js"></script>
    <script>
        CheckSessionStatus();
    </script>
    <script src="script/js/logout.js"></script>
    <script src="script/js/db-ops.js"></script>
    <script src="script/js/misc.js"></script>
    <script src="script/js/delete.js"></script>
    <link rel="stylesheet" href="themes/stylesheet/ui.css">
</head>
<body>
    <button type="button" id="btn-logout">LOG OUT</button>
    <button type="button" id="btn-back">BACK</button>
    <div class="container">
        <div class="delete-form-container">
            <section id="preview">
                <h3>
                    Preview:
                </h3>
                <p id="preview-data">
                    None
                </p>
            </div>
            <form class="delete-form">
                <div id="error">
                </div>
                <p>Entry:</p>
                <input type="number" id="entry" min="1">
                <button type="submit" id="btn-submit">DELETE ITEM</button>
            </form>
        </div>
    </div>
</body>
</html>