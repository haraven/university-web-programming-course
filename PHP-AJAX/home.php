<!DOCTYPE html>
<html>
<head>
    <title>WDB home</title>
    <script src="script/js/jquery-2.2.3.min.js"></script>
    <script src="script/js/jquery.session.js"></script>
    <script src="script/js/ui.js"></script>
    <script src="script/js/logout.js"></script>
    <script src="script/js/db-ops.js"></script>
    <script>
        CheckSessionStatus();
    </script>
    <link rel="stylesheet" href="themes/stylesheet/ui.css">
</head>
<body>
    <button type="button" id="btn-logout">Log out</button>
    <br />
    <div class="db-input-container">
        <p>
            'Item' table contains:
        </p>
        <div id="response-container">
        </div>
    <ul class="buttons">
        <li><a href="create.php" id="create-link">CREATE</a></li>
        <li><a href="update.php" id="update-link">UPDATE</a></li>
        <li><a href="delete.php" id="delete-link">DELETE</a></li>
    </ul>
    </div>
    </div>
</body>
</html>