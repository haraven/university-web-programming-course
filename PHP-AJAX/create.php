<!DOCTYPE html>
<html>
<head>
    <title>Create a new item</title>
    <script src="script/js/jquery-2.2.3.min.js"></script>
    <script src="script/js/ui.js"></script>
    <script>
        CheckSessionStatus();
    </script>
    <script src="script/js/logout.js"></script>
    <script src="script/js/db-ops.js"></script>
    <script src="script/js/misc.js"></script>
    <script src="script/js/create.js"></script>
    <link rel="stylesheet" href="themes/stylesheet/ui.css">
</head>
<body>
    <button type="button" id="btn-logout">LOG OUT</button>
    <button type="button" id="btn-back">BACK</button>
    <div class="container">
        <div class="input-form-container">
            <form class="input-form">
                <div id="error">
                </div>
                <p>Name:</p>
                <input type="text" id="name">
                <p>Displayid:</p>
                <input type="number" id="displayid" min="0" value="0">
                <p>Quality:</p>
                <input type="number" id="quality" min="0" max="5" value="0">
                <p>Buycount:</p>
                <input type="number" id="buycount" min="0" max="200" value="0">
                <p>Buyprice:</p>
                <input type="number" id="buyprice" min="0" value="0">
                <p>Sellprice:</p>
                <input type="number" id="sellprice" min="0" value="0">
                <button type="submit" id="btn-submit">CREATE ITEM</button>
            </form>
        </div>
    </div>
</body>
</html>