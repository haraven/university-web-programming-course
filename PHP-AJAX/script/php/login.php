<?php
session_start();
require_once 'dbconfig.php';
if (!isset($_SESSION['username']))
{
    $db_manager = new DBManager;
    if(isset($_GET['username']) && isset($_GET['password']))
    {     
        echo $db_manager->CheckUserLogin();
    }
    else if (isset($_POST['username']) && isset($_POST['password']))
    {
        echo $db_manager->CreateUser();
    }
    else
    {
        echo 'No username and/or password received';
    }
}
else
{
    echo 'Connection already open';
}
?>