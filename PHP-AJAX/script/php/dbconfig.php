<?php
if (session_status() == PHP_SESSION_NONE) 
{
    session_start();
}

class DBManager
{
    public $db_host = "localhost:3306";
    public $db_name = "wdb";
    private $db_username = "root";
    private $db_password = "RESILIENCEWILLFIXIT";

    function TryConnect($username, $password)
    {
        try
        {
            $db_manager = new PDO("mysql:host={$this->db_host};dbname={$this->db_name}", $username, $password);
            $db_manager->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $db_manager;
        }
        catch(PDOException $e)
        {
            return $e->getMessage();
        }
    }

    function GetDBName()
    {
        return $this->db_name;
    }

    function CheckUserLogin()
    {
        try
        {
            $username = $_GET['username'];
            $password = $_GET['password'];

            $db_connection = new PDO("mysql:host={$this->db_host};dbname={$this->db_name}", $this->db_username, $this->db_password);
            $db_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $statement = $db_connection->prepare("SELECT Password FROM user WHERE Username = :username");
            $statement->bindParam(':username', $username, PDO::PARAM_STR);

            $statement->execute();
            $row = $statement->fetchObject();

            if ($row != FALSE)
            {    
                if (strcmp($password, $row->Password) == 0)
                    return "OK";
                else
                    return "Invalid password";
            }
            else
                return "Invalid username";
        }
        catch (PDOException $e)
        {
            return $e->getMessage();
        }
    }

    function CreateUser()
    {
        try
        {
            $username = $_POST['username'];
            $password = $_POST['password'];
            $db_connection = new PDO("mysql:host={$this->db_host};dbname={$this->db_name}", $this->db_username, $this->db_password);
            $db_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $select = $db_connection->prepare("SELECT * FROM user WHERE Username = :username");
            $select->bindParam("username", $username, PDO::PARAM_STR);
            $select->execute();
            $row = $select->fetchObject();

            if ($row != FALSE)
                return "username already exists";

            $statement = $db_connection->prepare("INSERT INTO user (Username, Password) VALUES (:username, :password)");
            $statement->execute(array
            (
                "username" => $username,
                "password" => $password
            ));

            return "done";
        }
        catch (PDOException $e)
        {
            return $e->getMessage();
        }
    }

    function Create()
    {
        try
        {
            $name      = $_POST['name'];
            $displayid = $_POST['displayid'];
            $quality   = $_POST['quality'];
            $buycount  = $_POST['buycount'];
            $buyprice  = $_POST['buyprice'];
            $sellprice = $_POST['sellprice'];

            $username = $_SESSION['username'];
            $password = $_SESSION['password'];
            $db_connection = new PDO("mysql:host={$this->db_host};dbname={$this->db_name}", $username, $password);
            $db_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $statement = $db_connection->prepare("INSERT INTO item (name, displayid, quality, buycount, buyprice, sellprice) VALUES (:name, :displayid, :quality, :buycount, :buyprice, :sellprice)");
            $statement->execute(array
            (
                "name"      => $name,
                "displayid" => $displayid,
                "quality"   => $quality,
                "buycount"  => $buycount,
                "buyprice"  => $buyprice,
                "sellprice" => $sellprice
            ));

            return "done";
        }
        catch (PDOException $e)
        {
            return $e->getMessage();
        }
    }

    function Select()
    {
        $username = $_SESSION['username'];
        $password = $_SESSION['password'];
        $db_connection = new PDO("mysql:host={$this->db_host};dbname={$this->db_name}", $username, $password);
        $db_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "SELECT * FROM item";
        $html = "<table id=\"my-table\">
        <thead><tr>
        <th>entry</th>
        <th>name</th>
        <th>displayid</th>
        <th>quality</th>
        <th>buycount</th>
        <th>buyprice</th>
        <th>sellprice</th>
        </tr></thead>
        <tbody>";
        foreach($db_connection->query($sql) as $row)
        {
            $html .= "<tr>
            <td class=\"text-cell\">";
            $html .= "<a id=\"entry-link{$row['entry']}\" onclick='setEntry(this)' href=\"update.php\">{$row['entry']}</a>";
            $html .= "</td><td class=\"text-cell\">";
            $html .= "{$row['name']}";
            $html .= "</td><td class=\"text-cell\">";
            $html .= "{$row['displayid']}";
            $html .= "</td><td class=\"text-cell\">";
            $html .= "{$row['quality']}";
            $html .= "</td><td class=\"text-cell\">";
            $html .= "{$row['buycount']}";
            $html .= "</td><td class=\"text-cell\">";
            $html .= "{$row['buyprice']}";
            $html .= "</td><td class=\"text-cell\">";
            $html .= "{$row['sellprice']}";
            $html .= "</td></tr>";
        }
        $html .= "</tbody></table>";

        return $html;
    }

    function SelectWhereID()
    {
        $username = $_SESSION['username'];
        $password = $_SESSION['password'];
        $entry    = $_GET['entry'];
        $db_connection = new PDO("mysql:host={$this->db_host};dbname={$this->db_name}", $username, $password);
        $db_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $statement = $db_connection->prepare("SELECT * FROM item WHERE entry = :entry");
        $statement->bindParam(':entry', $entry, PDO::PARAM_INT);
        $statement->execute();
        $my_string = "";
        $row = $statement->fetchObject();
        if ($row != FALSE)
        {    
            $my_string = $row->entry . "|" . $row->name . "|" . $row->displayid . "|" . $row->quality . "|" . $row->buycount . "|" . $row->buyprice . "|" . $row->sellprice;
            return $my_string;        
        }
        else
            return "no result for entry " . $entry;
    }

    function Update()
    {
        try
        {
            $entry     = $_POST['entry'];
            $name      = $_POST['name'];
            $displayid = $_POST['displayid'];
            $quality   = $_POST['quality'];
            $buycount  = $_POST['buycount'];
            $buyprice  = $_POST['buyprice'];
            $sellprice = $_POST['sellprice'];

            $username = $_SESSION['username'];
            $password = $_SESSION['password'];
            $db_connection = new PDO("mysql:host={$this->db_host};dbname={$this->db_name}", $username, $password);
            $db_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $statement = $db_connection->prepare("UPDATE item SET name = :name, displayid = :displayid, quality = :quality, buycount = :buycount, buyprice = :buyprice, sellprice = :sellprice WHERE entry = :entry");

            $statement->execute(array
            (
                "name"      => $name,
                "displayid" => $displayid,
                "quality"   => $quality,
                "buycount"  => $buycount,
                "buyprice"  => $buyprice,
                "sellprice" => $sellprice,
                "entry"     => $entry,
            ));

            if ($statement->rowCount() > 0)
                return "done";
            else
                return "no entry found for id " + $entry;
        }
        catch (PDOException $e)
        {
            return $e->getMessage();
        }
    }

    function Delete()
    {
        try
        {
            $headers = getallheaders();
            $entry   = $headers['entry'];

            $username = $_SESSION['username'];
            $password = $_SESSION['password'];
            $db_connection = new PDO("mysql:host={$this->db_host};dbname={$this->db_name}", $username, $password);
            $db_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $statement = $db_connection->prepare("DELETE from item WHERE entry = :entry");
            $statement->execute(array
            (
                "entry" => $entry
            ));

            if ($statement->rowCount() > 0)
                return "done";
            else
                return "no entry found for id " . $entry;
        }
        catch (PDOException $e)
        {
            return $e->getMessage();
        }
    }
}    
?>