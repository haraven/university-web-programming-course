$("document").ready(function()
{
    Select();

    ("#")
});

function Select()
{
    xmlhttp = GetXMLHTTPObject();
    if (xmlhttp == null)
    {
        alert ("Browser doesn't support XMLHTTP.");
        return;
    }

    var url = "script/php/dbconfig.php?op=select";
    xmlhttp.onreadystatechange = function()
    {
        if (xmlhttp.readyState == 4)
        {
            if (xmlhttp.responseText == "Session expired")
            {   
                $("body").html("<p>Session has expired. You need to re-enter your credentials.</p>");
                window.location.href = "index.php";
            }
            
            $("#response-container").html(xmlhttp.responseText);
        }
    }
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}