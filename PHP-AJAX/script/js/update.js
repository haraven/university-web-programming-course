$('document').ready(function()
{
    var entry = $.session.get("entry");
    if (entry != undefined)
    {
        console.log(entry);
        $("#entry").val(entry);
        SelectWhere();
    }
    $(".update-form").submit(function(event)
    {
        var name = $("#name").val();
        if (name == "")
        {
            $("#error").fadeIn(1000, function()
            {
                $("#error").html("<div id=\"error\">Name cannot be null.</div>").fadeOut(4000);
            });

            return;
        }
        
        event.preventDefault();

        xmlhttp = GetXMLHTTPObject();
        if (xmlhttp == null)
        {
            alert ("Browser doesn't support XMLHTTP.");
            return;
        }

        var url = "script/php/dbconfig.php";
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4)
            {
                if (xmlhttp.responseText != "done")
                    $("#error").fadeIn(1000, function()
                    {
                        $("#error").html("<div id=\"error\">" + xmlhttp.responseText + "</div>").fadeOut(4000);
                    });
                else
                    $("#error").text("success!").fadeOut(4000);
            }
        };
        xmlhttp.open("POST", url, true);
        xmlhttp.send(GetUpdateFormData());
    });

    $("#entry").change(SelectWhere);
});

function GetUpdateFormData()
{
    var entry     = $("#entry").val();
    var name      = $("#name").val();
    var displayid = $("#displayid").val();
    var quality   = $("#quality").val();
    var buycount  = $("#buycount").val();
    var buyprice  = $("#buyprice").val();
    var sellprice = $("#sellprice").val();

    var form_data = new FormData();
    form_data.append("entry", entry);
    form_data.append("name", name);
    form_data.append("displayid", displayid);
    form_data.append("quality", quality);
    form_data.append("buycount", buycount);
    form_data.append("buyprice", buyprice);
    form_data.append("sellprice", sellprice);

    return form_data;
}

function SelectWhere()
{
    xmlhttp = GetXMLHTTPObject();
    if (xmlhttp == null)
    {
        alert ("Browser doesn't support XMLHTTP.");
        return;
    }

    var url = "script/php/dbconfig.php?op=select_where&entry=" + $("#entry").val();
    xmlhttp.onreadystatechange = function()
    {
        if (xmlhttp.readyState == 4)
        {
            if (xmlhttp.responseText == "Session expired")
            {   
                $("body").html("<p>Session has expired. You need to re-enter your credentials.</p>");
                window.location.href = "index.php";
            }
            else if (xmlhttp.responseText.indexOf("|") != -1)
            {
                var res = xmlhttp.responseText;
                var res_arr = res.split("|");
                $("#name").val(res_arr[1]);
                $("#displayid").val(res_arr[2]);
                $("#quality").val(res_arr[3]);
                $("#buycount").val(res_arr[4]);
                $("#buyprice").val(res_arr[5]);
                $("#sellprice").val(res_arr[6]);
            }
            else
            {
                $("#error").text(xmlhttp.responseText).fadeOut(4000);
                alert("Unexpected error: " + xmlhttp.responseText);
            }
        }
    }
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}