$(document).ready(function()
{
    $("#btn-login").click(OnLoginClicked);
    $("#btn-register").click(OnRegisterClicked);
});

function OnLoginClicked() 
{
    event.preventDefault();

    var username = $("#username").val();
    var password = $("#password").val();
    var errors = ValidateInput(username, password);
    if (errors != "")
    {
        $("#error").fadeIn(1000, function()
        {
            $("#error").html("<div id=\"error\">" + errors + "</div>").fadeOut(4000);
        });

        return;
    }

    $.ajax
    ({
        url: 'script/php/login.php',
        type: 'GET',
        data: 
        {
          username: username,
          password: password
        },
        success: function(data) 
        {
            if (data != "OK")
                $("#error").html("<div id=\"error\">" + data + "</div>").fadeOut(4000);       
            else
            {
                $.session.set('username', username);
                window.location.href = "home.php";
            }
        },
        error: function (xmlHttpRequest, textStatus, errorThrown) 
        {
            $("#error").html("<div id=\"error\">" + xmlHttpRequest.responseText + "</div>").fadeOut(4000);
        }
    });
}

function OnRegisterClicked()
{
    event.preventDefault();

    var username = $("#username").val();
    var password = $("#password").val();
    var errors = ValidateInput(username, password);
    if (errors != "")
    {
        $("#error").fadeIn(1000, function()
        {
            $("#error").html("<div id=\"error\">" + errors + "</div>").fadeOut(4000);
        });

        return;
    }

    $.ajax
    ({
        url: 'script/php/login.php',
        type: 'POST',
        data: 
        {
          username: username,
          password: password
        },
        success: function(data) 
        {
            if (data.indexOf("done") < 0)
                $("#error").html("<div id=\"error\">" + data + "</div>").fadeOut(4000);       
            else
            {
                $.session.set('username', username); 
                window.location.href = "home.php";
            }
        },
        error: function (xmlHttpRequest, textStatus, errorThrown) 
        {
            $("#error").html("<div id=\"error\">" + xmlHttpRequest.responseText + "</div>").fadeOut(4000);
        }
    });
}

function StateChanged()
{
    if (xmlhttp.readyState == 4)
        if (xmlhttp.responseText != "OK" && xmlhttp.responseText != "")
            $("#error").fadeIn(1000, function() 
            {
                $("#error").html("<div id=\"error\">" + xmlhttp.responseText + "</div>").fadeOut(4000);
            });
        else
        {
            $("body").html("<p>Login successful. Redirecting you to the homepage...</p>");
            window.location.href = "home.php";
        }
}

function ValidateInput(username, password)
{
    var errors = "";
    if (username == "" || username === undefined)
        errors += "Username cannot be null.";

    if (errors != "")
        errors += "<br>";

    if (password == "" || password === undefined)
        errors += "Password cannot be null."
    
    return errors;
}