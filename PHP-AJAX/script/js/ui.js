function GetFormData() 
{
    var username = $("#username").val();
    var password = $("#password").val();

    var form_data = new FormData();
    form_data.append("username", username);
    form_data.append("password", password);

    return form_data;
}

function GetXMLHTTPObject()
{
    if (window.XMLHttpRequest)
        return new XMLHttpRequest();
    if (window.ActiveXObject)
        return new ActiveXObject("Microsoft.XMLHTTP");
    return null;
}

function CheckSessionStatus()
{
    if ($.session.get('username') &&window.location.href.indexOf("index.php") >= 0)
        window.location.href = "home.php";
}

function GoBack()
{
    $("body").html("<p>Redirecting you to the homepage...</p>");
    window.location.href ="home.php";
}

function setEntry(my_val) {
    $.session.set("entry", my_val.innerHTML);
}