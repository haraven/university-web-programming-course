$('document').ready(function()
{
    $(".delete-form").submit(function(event)
    {
        var entry = $("#entry").val();
        if (entry === undefined || entry === "")
        {
            $("#error").text("Entry cannot be null").fadeOut(4000);
            return;
        }

        event.preventDefault();

        xmlhttp = GetXMLHTTPObject();
        if (xmlhttp == null)
        {
            alert ("Browser doesn't support XMLHTTP.");
            return;
        }

        var url = "script/php/dbconfig.php";
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4)
            {
                if (xmlhttp.responseText != "done")
                    $("#error").fadeIn(1000, function()
                    {
                        $("#error").html("<div id=\"error\">" + xmlhttp.responseText + "</div>").fadeOut(4000);
                    });
                else
                    $("#error").text("success!").fadeOut(4000);
            }
        };
        xmlhttp.open("DELETE", url, true);
        xmlhttp.setRequestHeader("entry", $("#entry").val());
        xmlhttp.send();
    });

    $("#entry").change(SelectWhere);
});

function GetDeleteFormData()
{
    var entry     = $("#entry").val();

    var form_data = new FormData();
    form_data.append("entry", entry);

    return form_data;
}

function SelectWhere()
{
    xmlhttp = GetXMLHTTPObject();
    if (xmlhttp == null)
    {
        alert ("Browser doesn't support XMLHTTP.");
        return;
    }

    var url = "script/php/dbconfig.php?op=select_where&entry=" + $("#entry").val();
    xmlhttp.onreadystatechange = function()
    {
        if (xmlhttp.readyState == 4)
        {
            if (xmlhttp.responseText == "Session expired")
            {   
                $("body").html("<p>Session has expired. You need to re-enter your credentials.</p>");
                window.location.href = "index.php";
            }
            else if (xmlhttp.responseText.indexOf("|") != -1)
            {
                var res = xmlhttp.responseText;
                $("#preview-data").text(res.split("|").join(" | "));
            }
            else
            {
                $("#preview-data").text("None");
            }
        }
    }
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}