<!DOCTYPE html>
<html>
<head>
    <title>wdbtest</title>
    <script src="script/js/jquery-2.2.3.min.js"></script>
    <script src="script/js/jquery.session.js"></script>
    <script src="script/js/ui.js"></script>
    <script>
        CheckSessionStatus();
    </script>
    <script src="script/js/login.js"></script>
    <link rel="stylesheet" href="themes/stylesheet/ui.css">
</head>
<body>
    <div class="container">
        <h1 class="welcome-msg">
            LOG IN
        </h1>
        <form id="login-form">
            <div id="error">
            </div>
            <div id="username-group">
                <span>
                    Username:
                </span>
                <br />
                <input type="text" id="username">
            </div>
            <br />
            <div id="password-group">
                <span>
                    Password:
                </span>
                <br />
                <input type="password" id="password">
            </div>
            <br />
            <button type="submit" id="btn-login">LOG IN</button>
            <button type="submit" id="btn-register">REGISTER</button>
        </form>
    </div>
</body>
</html>