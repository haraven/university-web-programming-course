﻿
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="home.aspx.cs" Inherits="ASP.home" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Home</title>
    <link rel="stylesheet" href="themes/css/ui.css" />
</head>
<body>
    <div class="container">
        <form id="main_form" runat="server">
            <div>
                <asp:ScriptManager ID="MainScriptManager" runat="server" EnablePageMethods="true">
                </asp:ScriptManager>

                <asp:Label ID="error" ForeColor="red" runat="server" />
                <br />
                <asp:UpdatePanel ID="doc_update_panel" UpdateMode="Conditional" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="item_grid" AutoGenerateColumns="false" runat="server">
                            <Columns>
                                <asp:HyperLinkField DataTextField="entry" DataNavigateUrlFields="entry" DataNavigateUrlFormatString="update.aspx?entry={0}" Text="entry" />
                                <asp:BoundField HeaderText="name" DataField="name" />
                                <asp:BoundField HeaderText="displayid" DataField="displayid" />
                                <asp:BoundField HeaderText="quality" DataField="quality" />
                                <asp:BoundField HeaderText="buycount" DataField="buycount" />
                                <asp:BoundField HeaderText="buyprice" DataField="buyprice" />
                                <asp:BoundField HeaderText="sellprice" DataField="sellprice" />
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <a href='<%# DataBinder.Eval(Container.DataItem, "entry", "home.aspx?entry={0}&action=delete") %>'>
                                            DELETE
                                        </a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <br />
                <br />

                <asp:Button CssClass="button" ID="btn_add" Text="ADD ITEM" OnClick="OnAddClicked" runat="server" />
            </div>
        </form>
    </div>
</body>
</html>
