﻿using System;
using MySql.Data.MySqlClient;

namespace ASP.controllers
{
    public class LoginController
    {
        public int Exists(string username, string password)
        {
            int entry_id = -1;
            try
            {
                using (var conn = new MySqlConnection("server=127.0.0.1;port=3306;uid=root;" +
                    "pwd=RESILIENCEWILLFIXIT;database=wdb;"))
                {
                    string sql = "SELECT * FROM wdb.user WHERE Username = @username AND Password = @password";
                    MySqlCommand select = new MySqlCommand(sql, conn);
                    select.Parameters.AddWithValue("@username", username);
                    select.Parameters.AddWithValue("@password", password);

                    conn.Open();

                    var reader = select.ExecuteReader();

                    if (reader.HasRows)
                    {
                        reader.Read();

                        entry_id = Convert.ToInt32(reader["EntryID"].ToString());
                    }

                    reader.Close();
                    conn.Close();
                }
            }
            catch (Exception)
            {
                return -1;
            }

            return entry_id;
        }

        public string Create(string username, string password)
        {
            try
            {
                using (var conn = new MySqlConnection("server=127.0.0.1;port=3306;uid=root;" +
                    "pwd=RESILIENCEWILLFIXIT;database=wdb;"))
                {
                    conn.Open();
                    string sql = "INSERT INTO wdb.user (Username, Password) VALUES (@username, @password)";
                    MySqlCommand select = new MySqlCommand(sql, conn);
                    select.Parameters.AddWithValue("@username", username);
                    select.Parameters.AddWithValue("@password", password);
                    select.ExecuteNonQuery();
                    conn.Close();
                }
            }
            catch (Exception e)
            {
                return e.Message;
            }


            return "success";
        }
    }
}