﻿using MySql.Data.MySqlClient;
using System;

namespace ASP
{
    public partial class _new : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["username"] == null)
                Response.Redirect("login.aspx");
        }

        protected void OnAddClicked(object sender, EventArgs e)
        {
            try
            {
                using (var sql_conn = new MySqlConnection("server=127.0.0.1;port=3306;uid=root;" +
                    "pwd=RESILIENCEWILLFIXIT;database=wdb;"))
                {
                    string sql = @"INSERT INTO wdb.item (name, displayid, quality, buycount, buyprice, sellprice)
                        VALUES (@name, @display_id, @quality, @buy_count, @buy_price, @sell_price)";

                    var insert = new MySqlCommand(sql, sql_conn);
                    insert.Parameters.AddWithValue("@name", name.Text);
                    insert.Parameters.AddWithValue("@display_id", displayid.Text);
                    insert.Parameters.AddWithValue("@quality", quality.Text);
                    insert.Parameters.AddWithValue("@buy_count", buycount.Text);
                    insert.Parameters.AddWithValue("@buy_price", buyprice.Text);
                    insert.Parameters.AddWithValue("@sell_price", sellprice.Text);

                    sql_conn.Open();

                    insert.ExecuteNonQuery();

                    sql_conn.Close();
                }
            }
            catch (Exception) { }

            Response.Redirect("home.aspx");
        }
    }
}