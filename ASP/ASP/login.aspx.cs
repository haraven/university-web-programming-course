﻿using ASP.controllers;
using System;

namespace ASP
{
    public partial class LoginWebForm : System.Web.UI.Page
    {
        protected void OnLoginClicked(object sender, EventArgs e)
        {
            var login_ctrl = new LoginController();

            var user_id = login_ctrl.Exists(username.Text,
                password.Text);

            if (user_id < 0)
            {
                error.Visible = true;
            }
            else
            {
                Session.Add("username", username.Text);
                Session.Add("password", password.Text);
                Response.Redirect("home.aspx");
            }
        }

        protected void OnRegisterClicked(object sender, EventArgs e)
        {
            var login_ctrl = new LoginController();

            string res = login_ctrl.Create(username.Text, password.Text);

            if (res.Equals("success"))
            {
                Session.Add("username", username.Text);
                Session.Add("password", password.Text);
                Response.Redirect("home.aspx");
            }
            else
            {
                error.Text = res;
                error.Visible = true;
            }
        }
    }
}