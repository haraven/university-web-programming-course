﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="update.aspx.cs" Inherits="ASP.update" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Update</title>
    <link rel="stylesheet" href="themes/css/ui.css" />
</head>
<body>
    <div class="container">
        <form id="update_form" runat="server">
        <div>
            <asp:RegularExpressionValidator ID="DisplayIDValidator" ControlToValidate="displayid" runat="server" ErrorMessage="Display ID must be a number" ValidationExpression="\d+"></asp:RegularExpressionValidator>
            <asp:RegularExpressionValidator ID="QualityValidator" ControlToValidate="quality" runat="server" ErrorMessage="Quality must be a number" ValidationExpression="\d+"></asp:RegularExpressionValidator>
            <asp:RegularExpressionValidator ID="BuyCountValidator" ControlToValidate="buycount" runat="server" ErrorMessage="Buy count must be a number" ValidationExpression="\d+"></asp:RegularExpressionValidator>
            <asp:RegularExpressionValidator ID="BuyPriceValidator" ControlToValidate="buyprice" runat="server" ErrorMessage="Buy price must be a number" ValidationExpression="\d+"></asp:RegularExpressionValidator>
            <asp:RegularExpressionValidator ID="SellPriceValidator" ControlToValidate="sellprice" runat="server" ErrorMessage="Sell price must be a number" ValidationExpression="\d+"></asp:RegularExpressionValidator>
            <asp:Label runat="server">Name</asp:Label>
            <br />
            <asp:TextBox ID="name" runat="server" />
            <br />
            <asp:Label runat="server">Display ID</asp:Label>
            <br />
            <asp:TextBox ID="displayid" runat="server" />
            <br />
            <asp:Label runat="server">Quality</asp:Label>
            <br />
            <asp:TextBox ID="quality" runat="server" />
            <br />
            <asp:Label runat="server">Buy Count</asp:Label>
            <br />
            <asp:TextBox ID="buycount" runat="server" />
            <br />
            <asp:Label runat="server">Buy Price</asp:Label>
            <br />
            <asp:TextBox ID="buyprice" runat="server" />
            <br />
            <asp:Label runat="server">Sell Price</asp:Label>
            <br />
            <asp:TextBox ID="sellprice" runat="server" />
            <br />
            <asp:Button CssClass="button" ID="btn_update" Text="UPDATE" OnClick="OnUpdateClicked" runat="server" />
        </div>
        </form>
    </div>
</body>
</html>
