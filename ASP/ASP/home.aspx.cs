﻿using System;
using MySql.Data.MySqlClient;
using System.Data;

namespace ASP
{
    public partial class home : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["username"] == null)
                    Response.Redirect("login.aspx");

                MainScriptManager.RegisterAsyncPostBackControl(item_grid);

                string entry = Request.QueryString["entry"];
                string action = Request.QueryString["action"];

                if (string.IsNullOrEmpty(action) || !action.Equals("delete"))
                    GetItems();
                else if (action.Equals("delete"))
                    DeleteItem(Convert.ToInt32(entry));                    
            }
        }

        private void GetItems()
        {
            try
            {
                using (var sql_conn = new MySqlConnection("server=127.0.0.1;port=3306;uid=root;" +
                    "pwd=RESILIENCEWILLFIXIT;database=wdb;"))
                {
                    sql_conn.Open();

                    var adapter = new MySqlDataAdapter("SELECT * FROM wdb.item", sql_conn);
                    var data_set = new DataSet();
                    adapter.Fill(data_set);

                    item_grid.DataSource = data_set;
                    item_grid.DataBind();

                    sql_conn.Close();
                }
            }
            catch(Exception e)
            {
                error.Text = e.Message;
            }
        }

        private void DeleteItem(int entry_id)
        {
            try
            {
                using (var sql_conn = new MySqlConnection("server=127.0.0.1;port=3306;uid=root;" +
                    "pwd=RESILIENCEWILLFIXIT;database=wdb;"))
                {
                    string sql = "DELETE FROM wdb.item WHERE entry = @entry_id";

                    sql_conn.Open();
                    var delete = new MySqlCommand(sql, sql_conn);
                    delete.Parameters.AddWithValue("@entry_id", entry_id);
                    delete.ExecuteNonQuery();

                    sql_conn.Close();
                }
            }
            catch (Exception e)
            {
                error.Text = e.Message;
            }

            Response.Redirect("home.aspx");
        }

        protected void OnAddClicked(object sender, EventArgs e)
        {
            Response.Redirect("add_item.aspx");
        }
    }
}