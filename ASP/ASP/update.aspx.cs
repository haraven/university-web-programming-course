﻿using MySql.Data.MySqlClient;
using System;

namespace ASP
{
    public partial class update : System.Web.UI.Page
    {
        private int entry_id = -1;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["username"] == null)
                Response.Redirect("login.aspx");

            string entry = Request.QueryString["entry"];
            if (string.IsNullOrEmpty(entry))
                Response.Redirect("home.aspx");
            else
            {
                entry_id = Convert.ToInt32(entry);
                if (!IsPostBack)
                    FillFields();
            }
        }

        private void FillFields()
        {
            try
            {
                using (var sql_conn = new MySqlConnection("server=127.0.0.1;port=3306;uid=root;" +
                    "pwd=RESILIENCEWILLFIXIT;database=wdb;"))
                {
                    string sql = "SELECT * FROM wdb.item WHERE entry = @entry_id";
                    var select = new MySqlCommand(sql, sql_conn);
                    select.Parameters.AddWithValue("@entry_id", entry_id);

                    sql_conn.Open();

                    var reader = select.ExecuteReader();
                    if (reader.HasRows)
                    {
                        reader.Read();

                        name.Text = reader.GetString("name");
                        displayid.Text = reader.GetInt32("displayid").ToString();
                        quality.Text = reader.GetInt32("quality").ToString();
                        buycount.Text = reader.GetInt32("buycount").ToString();
                        buyprice.Text = reader.GetInt32("buyprice").ToString();
                        sellprice.Text = reader.GetInt32("sellprice").ToString();
                    }
                }
            }
            catch (Exception)
            {
                Response.Redirect("home.aspx");
            }
        }

        protected void OnUpdateClicked(object sender, EventArgs e)
        {
            try
            {
                using (var sql_conn = new MySqlConnection("server=127.0.0.1;port=3306;uid=root;" +
                    "pwd=RESILIENCEWILLFIXIT;database=wdb;"))
                {
                    string sql = @"UPDATE wdb.item 
                        SET name = @name, displayid = @display_id, quality = @quality, buycount = @buy_count, buyprice = @buy_price, sellprice = @sell_price
                        WHERE entry = @entry_id";
                    var update = new MySqlCommand(sql, sql_conn);
                    update.Parameters.AddWithValue("@name", name.Text);
                    update.Parameters.AddWithValue("@display_id", displayid.Text);
                    update.Parameters.AddWithValue("@quality", quality.Text);   
                    update.Parameters.AddWithValue("@buy_count", buycount.Text);
                    update.Parameters.AddWithValue("@buy_price", buyprice.Text);
                    update.Parameters.AddWithValue("@sell_price", sellprice.Text);
                    update.Parameters.AddWithValue("@entry_id", entry_id);

                    sql_conn.Open();

                    update.ExecuteNonQuery();

                    sql_conn.Close();
                }
            }
            catch (Exception) {}

            Response.Redirect("home.aspx");
        }
    }
}