﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="ASP.LoginWebForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>LOG IN</title>
    <link rel="stylesheet" href="themes/css/ui.css" />
</head>
<body>
    <div class="container">
        <h1 class="welcome-msg">
            LOG IN
        </h1>
        <form id="login_form" runat="server">
            <div>
                <asp:ScriptManager id="MainScriptManager" runat="server" EnablePageMethods="true" />

                <asp:Label ID="error" Text="Invalid username or password." ForeColor="red" Visible="false" runat="server" />
                <div id="username-group">
                    <asp:Label runat="server">
                        Username:
                    </asp:Label>
                    <br />
                    <asp:TextBox id="username" runat="server" />
                </div>
                <br />
                <div id="password-group">
                    <asp:Label runat="server">
                        Password:
                    </asp:Label>
                    <br />
                    <asp:TextBox id="password" runat="server" />    
                </div>
                <br />
                <asp:Button CssClass="button" id="btn_login" Text="LOG IN" OnClick="OnLoginClicked" runat="server" />
                <asp:Button CssClass="button" id="btn_register" Text="REGISTER" OnClick="OnRegisterClicked" runat="server" />
            </div>
        </form>
    </div>
</body>
</html>
