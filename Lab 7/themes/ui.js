
// initialize array statically
var slideshow_images = [];
var img = new Image();
img.src = "resources/0.jpg";
slideshow_images.push(img);
img = new Image();
img.src = "resources/1.jpg";
slideshow_images.push(img);
img = new Image();
img.src = "resources/2.png";
slideshow_images.push(img);
img = new Image();
img.src = "resources/3.jpg";
slideshow_images.push(img);
img = new Image();
img.src = "resources/4.jpg";
slideshow_images.push(img);
img = new Image();
img.src = "resources/5.png";
slideshow_images.push(img);
img = new Image();
img.src = "resources/6.png";
slideshow_images.push(img);
img = new Image();
img.src = "resources/7.jpg";
slideshow_images.push(img);
img = new Image();
img.src = "resources/8.png";
slideshow_images.push(img);
img = new Image();
img.src = "resources/9.png";
slideshow_images.push(img);
img = new Image();
img.src = "resources/10.jpg";
slideshow_images.push(img);

// initialize slideshow-related variables
var current      = 0;
var delay        = 1; // delay in seconds
var timer        = null;
var auto_restart = false;

// event handler for the play button
function OnPlayClicked()
{
    if (current >= slideshow_images.length)
        current = 0;
    clearInterval(timer);
    timer = setInterval("SlideIt()", delay * 1000);
}

// event handler for the pause button
function OnPauseClicked()
{
    if (timer)
    {
        clearInterval(timer);
        timer = null;
    }
    else
    {
        timer = setInterval("SlideIt()", delay * 1000);
    }
}

// event handler for input text change (for changing slideshow transition delay)
function OnDelayChanged(value)
{
    delay = value;
    clearInterval(timer);
    timer = setInterval("SlideIt()", delay * 1000);
}

function Restart()
{
    auto_restart = !auto_restart;
    if (auto_restart && current >= slideshow_images.length)
    {
        current = 0;
        clearInterval(timer);
        timer = setInterval("SlideIt()", delay * 1000);
    }
}

function SlideIt()
{
    if (!document.images)
        return;

    if (current < slideshow_images.length)
    {
        var image = document.getElementById("slideshow-img");
        if (current < slideshow_images.length && image)
        {
            image.src = slideshow_images[current].src;
            current++;
        }
    }
    else if (auto_restart)
    {
        current = 0;
        SlideIt();
    }

}

function Load() 
{
    timer = setInterval("SlideIt()", delay * 1000);
    SlideIt();
}

// setup the slideshow after page loads
window.onload = Load();