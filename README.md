Course Contents
---------------
Client-side web technologies:
HTML;

HTTP;

CSS & CSS3;

XHTML, XML and XSLT;

Javascript and DOM;

jQuery and Angular.js;

WebGL.


Server-side web technologies:

CGI - Common Gateway Interface;

AJAX and PHP, JSON;

Jsp and Java Servlets, Tomcat;

ASP.NET.